package com.husen.myapp.web.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.domain.Banner;
import com.husen.myapp.domain.Goods;
import com.husen.myapp.domain.enumeration.GoodsStatus;
import com.husen.myapp.repository.WxGoodsRepository;
import com.husen.myapp.service.GoodsService;
import com.husen.myapp.service.dto.GoodsDTO;
import com.husen.myapp.service.mapper.GoodsMapper;
import com.husen.myapp.util.ShopConstant;

/**
 * REST controller for managing Goods.
 */
@RestController
@RequestMapping("/api")
public class WxGoodsResource {

    private final Logger log = LoggerFactory.getLogger(WxGoodsResource.class);

    private static final String ENTITY_NAME = "goods";

    private final GoodsService goodsService;
    
    @Autowired
    private WxGoodsRepository wxGoodsRepository;

    private final GoodsMapper goodsMapper;
    
    public WxGoodsResource(GoodsService goodsService, GoodsMapper goodsMapper) {
        this.goodsService = goodsService;
        this.goodsMapper = goodsMapper;
    }
    @GetMapping("/goods_list")
    @Timed 
    public ResponseEntity<Object> getGoods(@RequestParam(required=false) Integer status) {
        log.debug("REST request to get getGoods : {} userId", status);
        
       List<Goods> goods = new ArrayList<Goods>();
       
       if(status!=null){
    	   if(GoodsStatus.STATUSA.ordinal()==status.intValue()){
    		   goods = wxGoodsRepository.findByStatus(GoodsStatus.STATUSA);
    	   }else if(GoodsStatus.STATUSB.ordinal()==status.intValue()){
    		   goods = wxGoodsRepository.findByStatus(GoodsStatus.STATUSB);
    	   }
       }else{
    	   goods = wxGoodsRepository.findAll();
       }
       
       for(Goods good:goods) good.setPicImg(null);
       
        Map<String,Object> body = new HashMap<String,Object>();
       	body.put(ShopConstant.RESULT_CODE, ShopConstant.RESULT_CODE_SUCCESS);
       	
    	body.put(ShopConstant.RESULT_DATA, goods!=null?goodsMapper.toDto(goods):null);
    	
    	return ResponseEntity.ok().body(body);
    }

}
