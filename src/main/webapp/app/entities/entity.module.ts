import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HusenGoodsModule } from './goods/goods.module';
import { HusenShopModule } from './shop/shop.module';
import { HusenShopInfoModule } from './shop-info/shop-info.module';
import { HusenCouponsModule } from './coupons/coupons.module';
import { HusenUserInfoModule } from './user-info/user-info.module';
import { HusenBannerModule } from './banner/banner.module';
import { HusenOrderModule } from './order/order.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        HusenGoodsModule,
        HusenShopModule,
        HusenShopInfoModule,
        HusenCouponsModule,
        HusenUserInfoModule,
        HusenBannerModule,
        HusenOrderModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HusenEntityModule {}
