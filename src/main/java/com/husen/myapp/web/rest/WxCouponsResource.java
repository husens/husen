package com.husen.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.domain.Shop;
import com.husen.myapp.domain.User;
import com.husen.myapp.service.CouponsService;
import com.husen.myapp.web.rest.WxUserJWTController.JWTToken;
import com.husen.myapp.web.rest.errors.BadRequestAlertException;
import com.husen.myapp.web.rest.util.HeaderUtil;
import com.husen.myapp.web.rest.util.PaginationUtil;
import com.husen.myapp.service.dto.CouponsDTO;
import com.husen.myapp.util.ShopConstant;

import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Coupons.
 */
@RestController
@RequestMapping("/api")
public class WxCouponsResource {

    private final Logger log = LoggerFactory.getLogger(WxCouponsResource.class);

    private static final String ENTITY_NAME = "coupons";

    private final CouponsService couponsService;

    public WxCouponsResource(CouponsService couponsService) {
        this.couponsService = couponsService;
    }
    /**
     * 检查优惠券是否可用
     * @param userId
     * @param appid
     * @return
     */
    @GetMapping("/coupons_check")
    @Timed 
    public ResponseEntity<Object> getShopinfo(@RequestParam(required=false) Long id) {
        log.debug("REST request to get coupons_check : {} userId", id);
        
        Map<String,Object> body = new HashMap<String,Object>();
       	body.put(ShopConstant.RESULT_CODE, ShopConstant.RESULT_CODE_FAILURE);
    	return ResponseEntity.ok().body(body);
    }
}
