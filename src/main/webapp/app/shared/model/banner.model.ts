export const enum BannerType {
    HOME = 'HOME',
    MENU = 'MENU'
}

export interface IBanner {
    id?: number;
    name?: string;
    appid?: string;
    openid?: string;
    picUrl?: string;
    businessId?: number;
    type?: BannerType;
    picContentType?: string;
    pic?: any;
    shopId?: number;
}

export class Banner implements IBanner {
    constructor(
        public id?: number,
        public name?: string,
        public appid?: string,
        public openid?: string,
        public picUrl?: string,
        public businessId?: number,
        public type?: BannerType,
        public picContentType?: string,
        public pic?: any,
        public shopId?: number
    ) {}
}
