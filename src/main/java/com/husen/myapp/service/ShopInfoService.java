package com.husen.myapp.service;

import com.husen.myapp.service.dto.ShopInfoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing ShopInfo.
 */
public interface ShopInfoService {

    /**
     * Save a shopInfo.
     *
     * @param shopInfoDTO the entity to save
     * @return the persisted entity
     */
    ShopInfoDTO save(ShopInfoDTO shopInfoDTO);

    /**
     * Get all the shopInfos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ShopInfoDTO> findAll(Pageable pageable);
    /**
     * Get all the ShopInfoDTO where Shop is null.
     *
     * @return the list of entities
     */
    List<ShopInfoDTO> findAllWhereShopIsNull();


    /**
     * Get the "id" shopInfo.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ShopInfoDTO> findOne(Long id);

    /**
     * Delete the "id" shopInfo.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the shopInfo corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ShopInfoDTO> search(String query, Pageable pageable);
}
