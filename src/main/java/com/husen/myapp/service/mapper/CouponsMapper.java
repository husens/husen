package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.*;
import com.husen.myapp.service.dto.CouponsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Coupons and its DTO CouponsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CouponsMapper extends EntityMapper<CouponsDTO, Coupons> {



    default Coupons fromId(Long id) {
        if (id == null) {
            return null;
        }
        Coupons coupons = new Coupons();
        coupons.setId(id);
        return coupons;
    }
}
