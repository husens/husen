import { Route } from '@angular/router';

import { HeadernavbarComponent } from './headernavbar.component';

export const headernavbarRoute: Route = {
  path: '',
  component: HeadernavbarComponent,
  outlet: 'headernavbar'
};
