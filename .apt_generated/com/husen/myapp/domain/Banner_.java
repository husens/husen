package com.husen.myapp.domain;

import com.husen.myapp.domain.enumeration.BannerType;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Banner.class)
public abstract class Banner_ {

	public static volatile SingularAttribute<Banner, String> picUrl;
	public static volatile SingularAttribute<Banner, Shop> shop;
	public static volatile SingularAttribute<Banner, String> openid;
	public static volatile SingularAttribute<Banner, String> appid;
	public static volatile SingularAttribute<Banner, String> name;
	public static volatile SingularAttribute<Banner, Long> businessId;
	public static volatile SingularAttribute<Banner, String> picContentType;
	public static volatile SingularAttribute<Banner, Long> id;
	public static volatile SingularAttribute<Banner, byte[]> pic;
	public static volatile SingularAttribute<Banner, BannerType> type;

}

