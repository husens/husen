package com.husen.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ShopInfo.
 */
@Entity
@Table(name = "shop_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shopinfo")
public class ShopInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sname")
    private String sname;

    @Column(name = "gname")
    private String gname;

    @Column(name = "gadds")
    private String gadds;

    @Column(name = "home_type")
    private Integer homeType;

    @Column(name = "car_type")
    private Integer carType;

    @Column(name = "order_type")
    private Integer orderType;

    @Column(name = "vip_type")
    private Integer vipType;

    @Column(name = "vip_sale")
    private Integer vipSale;

    @Column(name = "spic")
    private String spic;

    @Column(name = "gpic")
    private String gpic;

    @Lob
    @Column(name = "spic_img")
    private byte[] spicImg;

    @Column(name = "spic_img_content_type")
    private String spicImgContentType;

    @Lob
    @Column(name = "gpic_img")
    private byte[] gpicImg;

    @Column(name = "gpic_img_content_type")
    private String gpicImgContentType;

    @Column(name = "search_tag")
    private String searchTag;

    @Column(name = "car_text")
    private String carText;

    @Column(name = "times")
    private String times;

    @Column(name = "phone")
    private String phone;

    @Column(name = "coupons")
    private Long coupons;

    @OneToOne    @JoinColumn(unique = true)
    private Coupons coupons1;

    @OneToOne(mappedBy = "shopInfo")
    @JsonIgnore
    private Shop shop;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSname() {
        return sname;
    }

    public ShopInfo sname(String sname) {
        this.sname = sname;
        return this;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getGname() {
        return gname;
    }

    public ShopInfo gname(String gname) {
        this.gname = gname;
        return this;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getGadds() {
        return gadds;
    }

    public ShopInfo gadds(String gadds) {
        this.gadds = gadds;
        return this;
    }

    public void setGadds(String gadds) {
        this.gadds = gadds;
    }

    public Integer getHomeType() {
        return homeType;
    }

    public ShopInfo homeType(Integer homeType) {
        this.homeType = homeType;
        return this;
    }

    public void setHomeType(Integer homeType) {
        this.homeType = homeType;
    }

    public Integer getCarType() {
        return carType;
    }

    public ShopInfo carType(Integer carType) {
        this.carType = carType;
        return this;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public ShopInfo orderType(Integer orderType) {
        this.orderType = orderType;
        return this;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getVipType() {
        return vipType;
    }

    public ShopInfo vipType(Integer vipType) {
        this.vipType = vipType;
        return this;
    }

    public void setVipType(Integer vipType) {
        this.vipType = vipType;
    }

    public Integer getVipSale() {
        return vipSale;
    }

    public ShopInfo vipSale(Integer vipSale) {
        this.vipSale = vipSale;
        return this;
    }

    public void setVipSale(Integer vipSale) {
        this.vipSale = vipSale;
    }

    public String getSpic() {
        return spic;
    }

    public ShopInfo spic(String spic) {
        this.spic = spic;
        return this;
    }

    public void setSpic(String spic) {
        this.spic = spic;
    }

    public String getGpic() {
        return gpic;
    }

    public ShopInfo gpic(String gpic) {
        this.gpic = gpic;
        return this;
    }

    public void setGpic(String gpic) {
        this.gpic = gpic;
    }

    public byte[] getSpicImg() {
        return spicImg;
    }

    public ShopInfo spicImg(byte[] spicImg) {
        this.spicImg = spicImg;
        return this;
    }

    public void setSpicImg(byte[] spicImg) {
        this.spicImg = spicImg;
    }

    public String getSpicImgContentType() {
        return spicImgContentType;
    }

    public ShopInfo spicImgContentType(String spicImgContentType) {
        this.spicImgContentType = spicImgContentType;
        return this;
    }

    public void setSpicImgContentType(String spicImgContentType) {
        this.spicImgContentType = spicImgContentType;
    }

    public byte[] getGpicImg() {
        return gpicImg;
    }

    public ShopInfo gpicImg(byte[] gpicImg) {
        this.gpicImg = gpicImg;
        return this;
    }

    public void setGpicImg(byte[] gpicImg) {
        this.gpicImg = gpicImg;
    }

    public String getGpicImgContentType() {
        return gpicImgContentType;
    }

    public ShopInfo gpicImgContentType(String gpicImgContentType) {
        this.gpicImgContentType = gpicImgContentType;
        return this;
    }

    public void setGpicImgContentType(String gpicImgContentType) {
        this.gpicImgContentType = gpicImgContentType;
    }

    public String getSearchTag() {
        return searchTag;
    }

    public ShopInfo searchTag(String searchTag) {
        this.searchTag = searchTag;
        return this;
    }

    public void setSearchTag(String searchTag) {
        this.searchTag = searchTag;
    }

    public String getCarText() {
        return carText;
    }

    public ShopInfo carText(String carText) {
        this.carText = carText;
        return this;
    }

    public void setCarText(String carText) {
        this.carText = carText;
    }

    public String getTimes() {
        return times;
    }

    public ShopInfo times(String times) {
        this.times = times;
        return this;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getPhone() {
        return phone;
    }

    public ShopInfo phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getCoupons() {
        return coupons;
    }

    public ShopInfo coupons(Long coupons) {
        this.coupons = coupons;
        return this;
    }

    public void setCoupons(Long coupons) {
        this.coupons = coupons;
    }

    public Coupons getCoupons1() {
        return coupons1;
    }

    public ShopInfo coupons1(Coupons coupons) {
        this.coupons1 = coupons;
        return this;
    }

    public void setCoupons1(Coupons coupons) {
        this.coupons1 = coupons;
    }

    public Shop getShop() {
        return shop;
    }

    public ShopInfo shop(Shop shop) {
        this.shop = shop;
        return this;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShopInfo shopInfo = (ShopInfo) o;
        if (shopInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shopInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShopInfo{" +
            "id=" + getId() +
            ", sname='" + getSname() + "'" +
            ", gname='" + getGname() + "'" +
            ", gadds='" + getGadds() + "'" +
            ", homeType=" + getHomeType() +
            ", carType=" + getCarType() +
            ", orderType=" + getOrderType() +
            ", vipType=" + getVipType() +
            ", vipSale=" + getVipSale() +
            ", spic='" + getSpic() + "'" +
            ", gpic='" + getGpic() + "'" +
            ", spicImg='" + getSpicImg() + "'" +
            ", spicImgContentType='" + getSpicImgContentType() + "'" +
            ", gpicImg='" + getGpicImg() + "'" +
            ", gpicImgContentType='" + getGpicImgContentType() + "'" +
            ", searchTag='" + getSearchTag() + "'" +
            ", carText='" + getCarText() + "'" +
            ", times='" + getTimes() + "'" +
            ", phone='" + getPhone() + "'" +
            ", coupons=" + getCoupons() +
            "}";
    }
}
