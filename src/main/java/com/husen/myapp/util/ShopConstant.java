package com.husen.myapp.util;

/**
 * Utility class for generating random Strings.
 */
public final class ShopConstant {

	//微信云存储
	
	public static final String QCLOUD_COS_APPID = "1252299811";	//开发者访问 COS 服务时拥有的用户维度唯一资源标识，用以标识资源
	
	public static final String QCLOUD_COS_SecretId = "AKIDCqeDgypXgCTWKrwSXN0HbLslQpjTcTwB";	//开发者拥有的项目身份识别 ID，用以身份认证
	
	public static final String QCLOUD_COS_SecretKey = "9I1WVfhaEukRwIUZPxGK9i2JMH6k13ye";	//开发者拥有的项目身份密钥
	
	public static final String QCLOUD_COS_Bucket = "husen-1252299811";	//COS 中用于存储数据的容器
	
	public static final String QCLOUD_COS_Object = null;	//COS 中存储的具体文件，是存储的基本实体
	
	public static final String QCLOUD_COS_Region = "ap-chengdu";	//域名中的地域信息。枚举值参见 可用地域 文档，如：ap-beijing, ap-hongkong, eu-frankfurt 等
	
	public static final String QCLOUD_COS_ACL = null;	//访问控制列表（Access Control List），是指特定 Bucket 或 Object 的访问控制信息列表
	
	public static final String QCLOUD_COS_CORS = null;	//跨域资源共享（Cross-Origin Resource Sharing），指发起请求的资源所在域不同于该请求所指向资源所在的域的 HTTP 请求
	
	public static final String QCLOUD_COS_Multipart = null;	//Uploads	分块上传，腾讯云 COS 服务为上传文件提供的一种分块上传模式
	
	public static final String QCLOUD_COS_SELL = "/sell"; //小店文件存储地址
	
	public static final String QCLOUD_COS_SELL_COLUMN_PIC = "pic"; //文件对象内字段名
	
	public static final String QCLOUD_COS_SELL_COLUMN_GPIC = "gpic"; //文件对象内字段名
	
	public static final String QCLOUD_COS_SELL_COLUMN_SPIC = "spic"; //文件对象内字段名
	
	public static final String QCLOUD_COS_SELL_FIELD_TYPE = ".png"; //图片文件类型
	
	public static final String QCLOUD_COS_RETURN_URL = "url"; //
	
	public static final String QCLOUD_COS_DOWNLOAD_URL = "https://"+QCLOUD_COS_Bucket+".cos."+QCLOUD_COS_Region+".myqcloud.com"; //微信云文件地址
	
	
	
	public static final String PART_lOGIN = "login";  
	public static final String PART_WXAPPLOGIN = "wxapplogin"; //登录
	public static final String PART_WXREGISTER = "register"; //注册
	public static final String PART_CHECKTOKEN = "checktoken"; //token效验
	public static final String PART_MLSHOPINFO = "shopinfo"; //店铺信息
	public static final String PART_MLSHOPSLIS = "shopinfo_shops"; //商铺列表
	public static final String PART_PRODERLIST = "order_delivery"; //订单配送列表
	public static final String PART_MLUSERINFO = "user_info"; //用户信息详情
	public static final String PART_USERPROFIT = "user_profit_record"; //用户收益记录
	public static final String PART_IMGSBANNEER = "banner_list"; //图片广告
	public static final String PART_MLTEXTAREA = "textarea_list"; //文字广告
	public static final String PART_MLCATEGORY = "shop_category"; //商品分类
	public static final String PART_CATEGORYDF = "category_detail"; //商品分类详情
	public static final String PART_MLGOODLIST = "goods_list"; //商品列表
	public static final String PART_NEWDAGOOS = "goods_newday"; //上新商品列表
	public static final String PART_MLGOODSDET = "goods_detail"; //商品详情
	public static final String PART_MLGHISTORY = "user_history_add"; //新增商品浏览记录
	public static final String PART_HISTORYLIS = "user_history_list"; //商品浏览记录列表
	public static final String PART_HISTORYDEL = "user_history_del"; //删除商品浏览记录
	public static final String PART_REPUTATION = "goods_reputation"; //商品评论
	public static final String PART_GOODSPRICE = "goods_price"; //查询商品价格
	public static final String PART_COUPONLIST = "coupons_list"; //优惠券列表
	public static final String PART_GETCOUPONS = "coupons_fetch"; //领取优惠券
	public static final String PART_MINCOUPONS = "my_coupons"; //用户领取的优惠券
	public static final String PART_CHECKUOPS = "coupons_check"; //检查优惠券是否可以领取
	public static final String PART_ORDERCOUPS = "order_coupons"; //检索订单可用优惠券
	public static final String PART_GETQRCODES = "goods_qrcode"; //生成小程序码
	public static final String PART_ADDRESLIST = "user_address_list"; //用户地址列表
	public static final String PART_ADDRESDEFA = "user_address"; //用户地址详情信息
	public static final String PART_ADDRESEDIT = "user_address_edit"; //编辑、新增用户地址信息
	public static final String PART_ADDRESSDEL = "user_address_del"; //删除用户地址信息
	public static final String PART_FAVORILIST = "user_favorite_list"; //收藏商品列表
	public static final String PART_FAVORIADDS = "user_favorite_add"; //添加收藏商品
	public static final String PART_FAVOREDELS = "user_favorite_del"; //删除收藏商品
	public static final String PART_ORDERCREATE = "order_create"; //创建订单
	public static final String PART_ORDERLIST = "order_list"; //订单列表
	public static final String PART_ORDERSTATS = "order_state_list"; //订单状态列表
	public static final String PART_ORDERPAYPAL = "order_paypal"; //订单支付
	public static final String PART_ORDERDETAS = "order_details"; //订单详情
	public static final String PART_ORDERCANCE = "order_cancel"; //订单取消
	public static final String PART_ORDERDELET = "order_delete"; //订单删除
	public static final String PART_ORDERECEIV = "order_receiving"; //订单确认收货
	public static final String PART_ORDEREFUND = "order_service_refund"; //申请退款
	public static final String PART_CANCELTUIK = "order_service_refund_cancel"; //撤销退款申请
	public static final String PART_RECOVERYTK = "order_service_refund_recovery"; //恢复退款订单状态
	public static final String PART_DELETETUIK = "order_service_refund_delete"; //删除退款订单
	public static final String PART_TUIKUANLIS = "order_service_refund_list"; //退款订单列表
	public static final String PART_EXCHANGLIS = "order_service_exchange_list"; //售后商品列表
	public static final String PART_EXCHANGESH = "order_service_exchange"; //申请售后
	public static final String PART_CANCELSHOU = "order_service_exchange_cancel"; //撤销售后申请
	public static final String PART_EXORDERLIS = "order_service_exchange_order_list"; //售后订单列表
	public static final String PART_DELEXCHANG = "order_service_exchange_delete"; //删除售后申请
	public static final String PART_ADDCHANGES = "order_service_exchange_addinfo"; //补充售后申请信息
	public static final String PART_PINGJALIST = "order_evaluate_list"; //订单评价商品列表
	public static final String PART_PINGJASEND = "order_evaluate"; //发表商品评价
	public static final String PART_WECHATPAYS = "get_wechat_paypal"; //微信支付
	public static final String PART_USERPAYPAL = "user_money_paypal"; //用户提现
	public static final String PART_USRECHARGE = "user_money_recharge"; //用户充值
	public static final String PART_USEPAYLIST = "get_paypal_list"; //充值赠送列表
	public static final String PART_PAYALINFO = "user_money_paypalinfo"; //用户余额记录
	public static final String PART_USFEEDBACK= "user_feedback"; //用户反馈
	public static final String PART_SCORERULES = "score_rules"; //积分签到规则
	public static final String PART_SCORESIGIN = "score_sign"; //每日签到
	public static final String PART_VIPOPAYPAL = "user_vip_paypal"; //开通、续费会员
	public static final String PART_WULIUINFOS = "order_wuliuinfo"; //查询物流信息
	public static final String PART_UPLOADFILE = "get_upload"; //文件上传
	public static final String PART_SHAREGOODS = "goods_share"; //商品分享返现


	public static final String RESULT_DATA = "data";
	
	public static final String RESULT_CODE = "code";
	
	public static final Integer RESULT_CODE_FAILURE = 1;
	
	public static final Integer RESULT_CODE_SUCCESS = 0;
	
	public static final Integer RESULT_CODE_ERROR = 2;
	
	public static final Integer RESULT_CODE_WARNING = 3;
	
	public static final Integer RESULT_CODE_1e4 = 10000;

	
	
	public static final String ENTITY_BANNER_TYPE_HOME = "home";
	
	public static final String ENTITY_BANNER_TYPE_MENU = "menu";
	
 

}
