package com.husen.myapp.web.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.husen.myapp.security.jwt.TokenProvider;
import com.husen.myapp.util.ShopConstant;
import com.husen.myapp.web.rest.vm.LoginVM;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class WxUserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    public WxUserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/login")
    @Timed
    public ResponseEntity<Object> login(@RequestParam String appid,@RequestParam String code) {
    	
    	LoginVM loginVM = new LoginVM();
    	loginVM.setPassword("admin");
    	loginVM.setUsername("admin");

    	UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());
    	
    	 Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
         SecurityContextHolder.getContext().setAuthentication(authentication);
         boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
         String jwt = tokenProvider.createToken(authentication, rememberMe);
         
    	
       	Map<String,Object> body = new HashMap<String,Object>();
       	body.put(ShopConstant.RESULT_CODE, ShopConstant.RESULT_CODE_1e4);
       	Map<String,Object> data = new HashMap<String,Object>();
       	data.put("uid", getUUID32());
       	data.put("token", new JWTToken(jwt).getIdToken());
       	body.put(ShopConstant.RESULT_DATA, data);
    	return ResponseEntity.ok().body(body);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
    
	public static String getUUID32(){
		String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
		return uuid;
	}
}
