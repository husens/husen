export interface ICoupons {
    id?: number;
    name?: string;
}

export class Coupons implements ICoupons {
    constructor(public id?: number, public name?: string) {}
}
