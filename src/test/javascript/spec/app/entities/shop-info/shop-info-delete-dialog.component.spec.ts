/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HusenTestModule } from '../../../test.module';
import { ShopInfoDeleteDialogComponent } from 'app/entities/shop-info/shop-info-delete-dialog.component';
import { ShopInfoService } from 'app/entities/shop-info/shop-info.service';

describe('Component Tests', () => {
    describe('ShopInfo Management Delete Component', () => {
        let comp: ShopInfoDeleteDialogComponent;
        let fixture: ComponentFixture<ShopInfoDeleteDialogComponent>;
        let service: ShopInfoService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HusenTestModule],
                declarations: [ShopInfoDeleteDialogComponent]
            })
                .overrideTemplate(ShopInfoDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ShopInfoDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ShopInfoService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
