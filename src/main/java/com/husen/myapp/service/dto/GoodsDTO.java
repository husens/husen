package com.husen.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import com.husen.myapp.domain.enumeration.GoodsStatus;

/**
 * A DTO for the Goods entity.
 */
public class GoodsDTO implements Serializable {

    private Long id;

    private String name;

    private String appid;

    private String openid;

    private String pic;

    @Lob
    private byte[] picImg;
    private String picImgContentType;

    private Long miniPrice;

    private Long orders;

    private GoodsStatus status;

    private Long shopId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public byte[] getPicImg() {
        return picImg;
    }

    public void setPicImg(byte[] picImg) {
        this.picImg = picImg;
    }

    public String getPicImgContentType() {
        return picImgContentType;
    }

    public void setPicImgContentType(String picImgContentType) {
        this.picImgContentType = picImgContentType;
    }

    public Long getMiniPrice() {
        return miniPrice;
    }

    public void setMiniPrice(Long miniPrice) {
        this.miniPrice = miniPrice;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    public GoodsStatus getStatus() {
        return status;
    }

    public void setStatus(GoodsStatus status) {
        this.status = status;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GoodsDTO goodsDTO = (GoodsDTO) o;
        if (goodsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), goodsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GoodsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", appid='" + getAppid() + "'" +
            ", openid='" + getOpenid() + "'" +
            ", pic='" + getPic() + "'" +
            ", picImg='" + getPicImg() + "'" +
            ", miniPrice=" + getMiniPrice() +
            ", orders=" + getOrders() +
            ", status='" + getStatus() + "'" +
            ", shop=" + getShopId() +
            "}";
    }
}
