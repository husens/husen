package com.husen.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.domain.Shop;
import com.husen.myapp.domain.User;
import com.husen.myapp.service.OrderService;
import com.husen.myapp.web.rest.errors.BadRequestAlertException;
import com.husen.myapp.web.rest.util.HeaderUtil;
import com.husen.myapp.web.rest.util.PaginationUtil;
import com.husen.myapp.service.dto.OrderDTO;
import com.husen.myapp.util.ShopConstant;

import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Order.
 */
@RestController
@RequestMapping("/api")
public class WxOrderResource {

    private final Logger log = LoggerFactory.getLogger(WxOrderResource.class);

    private static final String ENTITY_NAME = "order";

    private final OrderService orderService;

    public WxOrderResource(OrderService orderService) {
        this.orderService = orderService;
    }
    
    
    @GetMapping("/order_state_list")
    @Timed 
    public ResponseEntity<Map> getStates(@RequestParam String appid,@RequestParam String token) {
        log.debug("REST request to get getStates : {} appid", appid);
        log.debug("REST request to get getStates : {} token", token);
        
    	Map<String,Object> body = new HashMap<String,Object>();
    	
    	body.put(ShopConstant.RESULT_CODE, ShopConstant.RESULT_CODE_SUCCESS);
    	
    	
    	Map<String,Object> data = new HashMap<String,Object>();
    	
    	data.put("nopaypal", 2);
    	
    	body.put(ShopConstant.RESULT_DATA, data);
    	
    	
    	return ResponseEntity.ok().body(body);
    }
}
