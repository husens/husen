package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.Banner;
import com.husen.myapp.domain.Shop;
import com.husen.myapp.service.dto.BannerDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-01-22T18:53:44+0800",
    comments = "version: 1.2.0.Final, compiler: Eclipse JDT (IDE) 3.12.3.v20170228-1205, environment: Java 1.8.0_31 (Oracle Corporation)"
)
@Component
public class BannerMapperImpl implements BannerMapper {

    @Autowired
    private ShopMapper shopMapper;

    @Override
    public List<Banner> toEntity(List<BannerDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Banner> list = new ArrayList<Banner>( dtoList.size() );
        for ( BannerDTO bannerDTO : dtoList ) {
            list.add( toEntity( bannerDTO ) );
        }

        return list;
    }

    @Override
    public List<BannerDTO> toDto(List<Banner> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<BannerDTO> list = new ArrayList<BannerDTO>( entityList.size() );
        for ( Banner banner : entityList ) {
            list.add( toDto( banner ) );
        }

        return list;
    }

    @Override
    public BannerDTO toDto(Banner banner) {
        if ( banner == null ) {
            return null;
        }

        BannerDTO bannerDTO = new BannerDTO();

        Long id = bannerShopId( banner );
        if ( id != null ) {
            bannerDTO.setShopId( id );
        }
        bannerDTO.setId( banner.getId() );
        bannerDTO.setName( banner.getName() );
        bannerDTO.setAppid( banner.getAppid() );
        bannerDTO.setOpenid( banner.getOpenid() );
        bannerDTO.setPicUrl( banner.getPicUrl() );
        bannerDTO.setBusinessId( banner.getBusinessId() );
        bannerDTO.setType( banner.getType() );
        byte[] pic = banner.getPic();
        if ( pic != null ) {
            bannerDTO.setPic( Arrays.copyOf( pic, pic.length ) );
        }
        bannerDTO.setPicContentType( banner.getPicContentType() );

        return bannerDTO;
    }

    @Override
    public Banner toEntity(BannerDTO bannerDTO) {
        if ( bannerDTO == null ) {
            return null;
        }

        Banner banner = new Banner();

        banner.setShop( shopMapper.fromId( bannerDTO.getShopId() ) );
        banner.setId( bannerDTO.getId() );
        banner.setName( bannerDTO.getName() );
        banner.setAppid( bannerDTO.getAppid() );
        banner.setOpenid( bannerDTO.getOpenid() );
        banner.setPicUrl( bannerDTO.getPicUrl() );
        banner.setBusinessId( bannerDTO.getBusinessId() );
        banner.setType( bannerDTO.getType() );
        byte[] pic = bannerDTO.getPic();
        if ( pic != null ) {
            banner.setPic( Arrays.copyOf( pic, pic.length ) );
        }
        banner.setPicContentType( bannerDTO.getPicContentType() );

        return banner;
    }

    private Long bannerShopId(Banner banner) {
        if ( banner == null ) {
            return null;
        }
        Shop shop = banner.getShop();
        if ( shop == null ) {
            return null;
        }
        Long id = shop.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
