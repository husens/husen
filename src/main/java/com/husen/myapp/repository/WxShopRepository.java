package com.husen.myapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.husen.myapp.domain.Shop;
import com.husen.myapp.domain.User;


/**
 * Spring Data  repository for the Shop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WxShopRepository extends ShopRepository {
	Optional<Shop> findByUser(User user);
}
