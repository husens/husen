package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.*;
import com.husen.myapp.service.dto.ShopDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Shop and its DTO ShopDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ShopInfoMapper.class})
public interface ShopMapper extends EntityMapper<ShopDTO, Shop> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "shopInfo.id", target = "shopInfoId")
    ShopDTO toDto(Shop shop);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "shopInfoId", target = "shopInfo")
    @Mapping(target = "goods", ignore = true)
    @Mapping(target = "banners", ignore = true)
    Shop toEntity(ShopDTO shopDTO);

    default Shop fromId(Long id) {
        if (id == null) {
            return null;
        }
        Shop shop = new Shop();
        shop.setId(id);
        return shop;
    }
}
