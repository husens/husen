import { IGoods } from 'app/shared/model//goods.model';
import { IBanner } from 'app/shared/model//banner.model';

export interface IShop {
    id?: number;
    name?: string;
    userId?: number;
    shopInfoId?: number;
    goods?: IGoods[];
    banners?: IBanner[];
}

export class Shop implements IShop {
    constructor(
        public id?: number,
        public name?: string,
        public userId?: number,
        public shopInfoId?: number,
        public goods?: IGoods[],
        public banners?: IBanner[]
    ) {}
}
