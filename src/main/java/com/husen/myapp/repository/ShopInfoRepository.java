package com.husen.myapp.repository;

import com.husen.myapp.domain.ShopInfo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ShopInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopInfoRepository extends JpaRepository<ShopInfo, Long> {

}
