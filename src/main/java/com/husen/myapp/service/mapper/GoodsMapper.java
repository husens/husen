package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.*;
import com.husen.myapp.service.dto.GoodsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Goods and its DTO GoodsDTO.
 */
@Mapper(componentModel = "spring", uses = {ShopMapper.class})
public interface GoodsMapper extends EntityMapper<GoodsDTO, Goods> {

    @Mapping(source = "shop.id", target = "shopId")
    GoodsDTO toDto(Goods goods);

    @Mapping(target = "order1s", ignore = true)
    @Mapping(source = "shopId", target = "shop")
    Goods toEntity(GoodsDTO goodsDTO);

    default Goods fromId(Long id) {
        if (id == null) {
            return null;
        }
        Goods goods = new Goods();
        goods.setId(id);
        return goods;
    }
}
