import { Moment } from 'moment';

export interface IUserInfo {
    id?: number;
    vipLevelName?: string;
    vipState?: number;
    vipSale?: number;
    vipTime?: Moment;
    conSume?: number;
    userId?: number;
}

export class UserInfo implements IUserInfo {
    constructor(
        public id?: number,
        public vipLevelName?: string,
        public vipState?: number,
        public vipSale?: number,
        public vipTime?: Moment,
        public conSume?: number,
        public userId?: number
    ) {}
}
