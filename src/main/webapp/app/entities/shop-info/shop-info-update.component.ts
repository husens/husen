import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IShopInfo } from 'app/shared/model/shop-info.model';
import { ShopInfoService } from './shop-info.service';
import { ICoupons } from 'app/shared/model/coupons.model';
import { CouponsService } from 'app/entities/coupons';
import { IShop } from 'app/shared/model/shop.model';
import { ShopService } from 'app/entities/shop';

@Component({
    selector: 'jhi-shop-info-update',
    templateUrl: './shop-info-update.component.html'
})
export class ShopInfoUpdateComponent implements OnInit {
    shopInfo: IShopInfo;
    isSaving: boolean;

    coupons1s: ICoupons[];

    shops: IShop[];

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected shopInfoService: ShopInfoService,
        protected couponsService: CouponsService,
        protected shopService: ShopService,
        protected elementRef: ElementRef,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ shopInfo }) => {
            this.shopInfo = shopInfo;
        });
        this.couponsService.query({ filter: 'shopinfo-is-null' }).subscribe(
            (res: HttpResponse<ICoupons[]>) => {
                if (!this.shopInfo.coupons1Id) {
                    this.coupons1s = res.body;
                } else {
                    this.couponsService.find(this.shopInfo.coupons1Id).subscribe(
                        (subRes: HttpResponse<ICoupons>) => {
                            this.coupons1s = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.shopService.query().subscribe(
            (res: HttpResponse<IShop[]>) => {
                this.shops = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.shopInfo, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.shopInfo.id !== undefined) {
            this.subscribeToSaveResponse(this.shopInfoService.update(this.shopInfo));
        } else {
            this.subscribeToSaveResponse(this.shopInfoService.create(this.shopInfo));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IShopInfo>>) {
        result.subscribe((res: HttpResponse<IShopInfo>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCouponsById(index: number, item: ICoupons) {
        return item.id;
    }

    trackShopById(index: number, item: IShop) {
        return item.id;
    }
}
