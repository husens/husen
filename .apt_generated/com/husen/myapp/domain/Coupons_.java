package com.husen.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Coupons.class)
public abstract class Coupons_ {

	public static volatile SingularAttribute<Coupons, String> name;
	public static volatile SingularAttribute<Coupons, Long> id;

}

