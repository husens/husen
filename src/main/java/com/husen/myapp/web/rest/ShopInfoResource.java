package com.husen.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.service.ShopInfoService;
import com.husen.myapp.service.WxCloudService;
import com.husen.myapp.web.rest.errors.BadRequestAlertException;
import com.husen.myapp.web.rest.util.HeaderUtil;
import com.husen.myapp.web.rest.util.PaginationUtil;
import com.husen.myapp.service.dto.ShopInfoDTO;
import com.husen.myapp.util.ShopConstant;

import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShopInfo.
 */
@RestController
@RequestMapping("/api")
public class ShopInfoResource {

    private final Logger log = LoggerFactory.getLogger(ShopInfoResource.class);

    private static final String ENTITY_NAME = "shopInfo";

    private final ShopInfoService shopInfoService;

    @Autowired
    private WxCloudService wxCloudService;
    
    public ShopInfoResource(ShopInfoService shopInfoService) {
        this.shopInfoService = shopInfoService;
    }

    /**
     * POST  /shop-infos : Create a new shopInfo.
     *
     * @param shopInfoDTO the shopInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shopInfoDTO, or with status 400 (Bad Request) if the shopInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shop-infos")
    @Timed
    public ResponseEntity<ShopInfoDTO> createShopInfo(@RequestBody ShopInfoDTO shopInfoDTO) throws URISyntaxException {
        log.debug("REST request to save ShopInfo : {}", shopInfoDTO);
        if (shopInfoDTO.getId() != null) {
            throw new BadRequestAlertException("A new shopInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopInfoDTO result = shopInfoService.save(shopInfoDTO);
        
        Map r = wxCloudService.uploadFile(ENTITY_NAME, result.getId(),ShopConstant.QCLOUD_COS_SELL_COLUMN_GPIC, result.getGpicImg());
        
        result.setGpic(r.get(ShopConstant.QCLOUD_COS_RETURN_URL).toString());
        
        r = wxCloudService.uploadFile(ENTITY_NAME, result.getId(),ShopConstant.QCLOUD_COS_SELL_COLUMN_SPIC, result.getSpicImg());
        
        result.setSpic(r.get(ShopConstant.QCLOUD_COS_RETURN_URL).toString());
        
        result = shopInfoService.save(result);
        
        return ResponseEntity.created(new URI("/api/shop-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /shop-infos : Updates an existing shopInfo.
     *
     * @param shopInfoDTO the shopInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shopInfoDTO,
     * or with status 400 (Bad Request) if the shopInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the shopInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shop-infos")
    @Timed
    public ResponseEntity<ShopInfoDTO> updateShopInfo(@RequestBody ShopInfoDTO shopInfoDTO) throws URISyntaxException {
        log.debug("REST request to update ShopInfo : {}", shopInfoDTO);
        if (shopInfoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShopInfoDTO result = shopInfoService.save(shopInfoDTO);
        
        Map r = wxCloudService.uploadFile(ENTITY_NAME, result.getId(),ShopConstant.QCLOUD_COS_SELL_COLUMN_GPIC, result.getGpicImg());
        
        result.setGpic(r.get(ShopConstant.QCLOUD_COS_RETURN_URL).toString());
        
        r = wxCloudService.uploadFile(ENTITY_NAME, result.getId(),ShopConstant.QCLOUD_COS_SELL_COLUMN_SPIC, result.getSpicImg());
        
        result.setSpic(r.get(ShopConstant.QCLOUD_COS_RETURN_URL).toString());
        
        result = shopInfoService.save(result);
        
        
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shopInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /shop-infos : get all the shopInfos.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of shopInfos in body
     */
    @GetMapping("/shop-infos")
    @Timed
    public ResponseEntity<List<ShopInfoDTO>> getAllShopInfos(Pageable pageable, @RequestParam(required = false) String filter) {
        if ("shop-is-null".equals(filter)) {
            log.debug("REST request to get all ShopInfos where shop is null");
            return new ResponseEntity<>(shopInfoService.findAllWhereShopIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of ShopInfos");
        Page<ShopInfoDTO> page = shopInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shop-infos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /shop-infos/:id : get the "id" shopInfo.
     *
     * @param id the id of the shopInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shopInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shop-infos/{id}")
    @Timed
    public ResponseEntity<ShopInfoDTO> getShopInfo(@PathVariable Long id) {
        log.debug("REST request to get ShopInfo : {}", id);
        Optional<ShopInfoDTO> shopInfoDTO = shopInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shopInfoDTO);
    }

    /**
     * DELETE  /shop-infos/:id : delete the "id" shopInfo.
     *
     * @param id the id of the shopInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shop-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteShopInfo(@PathVariable Long id) {
        log.debug("REST request to delete ShopInfo : {}", id);
        shopInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shop-infos?query=:query : search for the shopInfo corresponding
     * to the query.
     *
     * @param query the query of the shopInfo search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shop-infos")
    @Timed
    public ResponseEntity<List<ShopInfoDTO>> searchShopInfos(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ShopInfos for query {}", query);
        Page<ShopInfoDTO> page = shopInfoService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shop-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
