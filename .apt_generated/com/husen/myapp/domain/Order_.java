package com.husen.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Order.class)
public abstract class Order_ {

	public static volatile SingularAttribute<Order, String> openid;
	public static volatile SingularAttribute<Order, String> appid;
	public static volatile SingularAttribute<Order, String> name;
	public static volatile SingularAttribute<Order, Goods> goods;
	public static volatile SingularAttribute<Order, Long> id;

}

