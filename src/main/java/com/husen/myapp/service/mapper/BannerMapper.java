package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.*;
import com.husen.myapp.service.dto.BannerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Banner and its DTO BannerDTO.
 */
@Mapper(componentModel = "spring", uses = {ShopMapper.class})
public interface BannerMapper extends EntityMapper<BannerDTO, Banner> {

    @Mapping(source = "shop.id", target = "shopId")
    BannerDTO toDto(Banner banner);

    @Mapping(source = "shopId", target = "shop")
    Banner toEntity(BannerDTO bannerDTO);

    default Banner fromId(Long id) {
        if (id == null) {
            return null;
        }
        Banner banner = new Banner();
        banner.setId(id);
        return banner;
    }
}
