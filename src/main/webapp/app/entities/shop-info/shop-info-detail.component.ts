import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IShopInfo } from 'app/shared/model/shop-info.model';

@Component({
    selector: 'jhi-shop-info-detail',
    templateUrl: './shop-info-detail.component.html'
})
export class ShopInfoDetailComponent implements OnInit {
    shopInfo: IShopInfo;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ shopInfo }) => {
            this.shopInfo = shopInfo;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
