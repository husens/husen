package com.husen.myapp.domain;

import com.husen.myapp.domain.enumeration.GoodsStatus;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Goods.class)
public abstract class Goods_ {

	public static volatile SingularAttribute<Goods, String> picImgContentType;
	public static volatile SingularAttribute<Goods, Shop> shop;
	public static volatile SingularAttribute<Goods, String> openid;
	public static volatile SingularAttribute<Goods, String> appid;
	public static volatile SingularAttribute<Goods, Long> miniPrice;
	public static volatile SingularAttribute<Goods, String> name;
	public static volatile SingularAttribute<Goods, Long> orders;
	public static volatile SingularAttribute<Goods, Long> id;
	public static volatile SingularAttribute<Goods, String> pic;
	public static volatile SetAttribute<Goods, Order> order1s;
	public static volatile SingularAttribute<Goods, byte[]> picImg;
	public static volatile SingularAttribute<Goods, GoodsStatus> status;

}

