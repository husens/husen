package com.husen.myapp.repository;

import com.husen.myapp.domain.Coupons;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Coupons entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CouponsRepository extends JpaRepository<Coupons, Long> {

}
