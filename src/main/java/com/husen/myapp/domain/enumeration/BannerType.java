package com.husen.myapp.domain.enumeration;

/**
 * The BannerType enumeration.
 */
public enum BannerType {
    HOME, MENU
}
