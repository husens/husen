package com.husen.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.service.CouponsService;
import com.husen.myapp.web.rest.errors.BadRequestAlertException;
import com.husen.myapp.web.rest.util.HeaderUtil;
import com.husen.myapp.web.rest.util.PaginationUtil;
import com.husen.myapp.service.dto.CouponsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Coupons.
 */
@RestController
@RequestMapping("/api")
public class CouponsResource {

    private final Logger log = LoggerFactory.getLogger(CouponsResource.class);

    private static final String ENTITY_NAME = "coupons";

    private final CouponsService couponsService;

    public CouponsResource(CouponsService couponsService) {
        this.couponsService = couponsService;
    }

    /**
     * POST  /coupons : Create a new coupons.
     *
     * @param couponsDTO the couponsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new couponsDTO, or with status 400 (Bad Request) if the coupons has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/coupons")
    @Timed
    public ResponseEntity<CouponsDTO> createCoupons(@RequestBody CouponsDTO couponsDTO) throws URISyntaxException {
        log.debug("REST request to save Coupons : {}", couponsDTO);
        if (couponsDTO.getId() != null) {
            throw new BadRequestAlertException("A new coupons cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CouponsDTO result = couponsService.save(couponsDTO);
        return ResponseEntity.created(new URI("/api/coupons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /coupons : Updates an existing coupons.
     *
     * @param couponsDTO the couponsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated couponsDTO,
     * or with status 400 (Bad Request) if the couponsDTO is not valid,
     * or with status 500 (Internal Server Error) if the couponsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/coupons")
    @Timed
    public ResponseEntity<CouponsDTO> updateCoupons(@RequestBody CouponsDTO couponsDTO) throws URISyntaxException {
        log.debug("REST request to update Coupons : {}", couponsDTO);
        if (couponsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CouponsDTO result = couponsService.save(couponsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, couponsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /coupons : get all the coupons.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of coupons in body
     */
    @GetMapping("/coupons")
    @Timed
    public ResponseEntity<List<CouponsDTO>> getAllCoupons(Pageable pageable) {
        log.debug("REST request to get a page of Coupons");
        Page<CouponsDTO> page = couponsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/coupons");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /coupons/:id : get the "id" coupons.
     *
     * @param id the id of the couponsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the couponsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/coupons/{id}")
    @Timed
    public ResponseEntity<CouponsDTO> getCoupons(@PathVariable Long id) {
        log.debug("REST request to get Coupons : {}", id);
        Optional<CouponsDTO> couponsDTO = couponsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(couponsDTO);
    }

    /**
     * DELETE  /coupons/:id : delete the "id" coupons.
     *
     * @param id the id of the couponsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/coupons/{id}")
    @Timed
    public ResponseEntity<Void> deleteCoupons(@PathVariable Long id) {
        log.debug("REST request to delete Coupons : {}", id);
        couponsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/coupons?query=:query : search for the coupons corresponding
     * to the query.
     *
     * @param query the query of the coupons search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/coupons")
    @Timed
    public ResponseEntity<List<CouponsDTO>> searchCoupons(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Coupons for query {}", query);
        Page<CouponsDTO> page = couponsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/coupons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
