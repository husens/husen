package com.husen.myapp.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.husen.myapp.domain.Banner;
import com.husen.myapp.domain.enumeration.BannerType;


/**
 * Spring Data  repository for the Banner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WxBannerRepository extends BannerRepository {
	
	List<Banner> findByType( BannerType type);
}
