import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IBanner } from 'app/shared/model/banner.model';

@Component({
    selector: 'jhi-banner-detail',
    templateUrl: './banner-detail.component.html'
})
export class BannerDetailComponent implements OnInit {
    banner: IBanner;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ banner }) => {
            this.banner = banner;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
