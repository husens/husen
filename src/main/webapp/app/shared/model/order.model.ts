export interface IOrder {
    id?: number;
    name?: string;
    appid?: string;
    openid?: string;
    goodsId?: number;
}

export class Order implements IOrder {
    constructor(public id?: number, public name?: string, public appid?: string, public openid?: string, public goodsId?: number) {}
}
