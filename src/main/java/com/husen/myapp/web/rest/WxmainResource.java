package com.husen.myapp.web.rest;

import java.net.URISyntaxException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codahale.metrics.annotation.Timed;

/**
 * REST controller for managing Goods.
 */
@RestController
@RequestMapping("/api")
public class WxmainResource {

    private final Logger log = LoggerFactory.getLogger(WxmainResource.class);


    public WxmainResource() {

    }

    /**
     * Get  /shop :  微信小店主入口
     *
     * @param userId 会员Id
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping("/{userId}")
    @Timed
    public ModelAndView  main(@PathVariable Long userId,@RequestParam String viewid,@RequestParam String appid,@RequestParam String part,HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) throws URISyntaxException {
        log.debug("REST request to shop main : {}", userId);
       
        redirectAttributes.addAttribute("userId", userId);
        Enumeration e = request.getParameterNames();
        while(e.hasMoreElements()){
        	String parameterName = (String)e.nextElement();
           redirectAttributes.addAttribute(parameterName, request.getParameter(parameterName));
        }
        
        return new ModelAndView("redirect:/api/"+part);
    }

}
