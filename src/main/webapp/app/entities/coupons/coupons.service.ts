import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICoupons } from 'app/shared/model/coupons.model';

type EntityResponseType = HttpResponse<ICoupons>;
type EntityArrayResponseType = HttpResponse<ICoupons[]>;

@Injectable({ providedIn: 'root' })
export class CouponsService {
    public resourceUrl = SERVER_API_URL + 'api/coupons';
    public resourceSearchUrl = SERVER_API_URL + 'api/_search/coupons';

    constructor(protected http: HttpClient) {}

    create(coupons: ICoupons): Observable<EntityResponseType> {
        return this.http.post<ICoupons>(this.resourceUrl, coupons, { observe: 'response' });
    }

    update(coupons: ICoupons): Observable<EntityResponseType> {
        return this.http.put<ICoupons>(this.resourceUrl, coupons, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICoupons>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICoupons[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICoupons[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
