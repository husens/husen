package com.husen.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Shop.class)
public abstract class Shop_ {

	public static volatile SingularAttribute<Shop, ShopInfo> shopInfo;
	public static volatile SingularAttribute<Shop, String> name;
	public static volatile SetAttribute<Shop, Goods> goods;
	public static volatile SingularAttribute<Shop, Long> id;
	public static volatile SingularAttribute<Shop, User> user;
	public static volatile SetAttribute<Shop, Banner> banners;

}

