package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.*;
import com.husen.myapp.service.dto.OrderDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Order and its DTO OrderDTO.
 */
@Mapper(componentModel = "spring", uses = {GoodsMapper.class})
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {

    @Mapping(source = "goods.id", target = "goodsId")
    OrderDTO toDto(Order order);

    @Mapping(source = "goodsId", target = "goods")
    Order toEntity(OrderDTO orderDTO);

    default Order fromId(Long id) {
        if (id == null) {
            return null;
        }
        Order order = new Order();
        order.setId(id);
        return order;
    }
}
