import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HusenSharedModule } from 'app/shared';
import {
    BannerComponent,
    BannerDetailComponent,
    BannerUpdateComponent,
    BannerDeletePopupComponent,
    BannerDeleteDialogComponent,
    bannerRoute,
    bannerPopupRoute
} from './';

const ENTITY_STATES = [...bannerRoute, ...bannerPopupRoute];

@NgModule({
    imports: [HusenSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [BannerComponent, BannerDetailComponent, BannerUpdateComponent, BannerDeleteDialogComponent, BannerDeletePopupComponent],
    entryComponents: [BannerComponent, BannerUpdateComponent, BannerDeleteDialogComponent, BannerDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HusenBannerModule {}
