package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.Goods;
import com.husen.myapp.domain.Order;
import com.husen.myapp.service.dto.OrderDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-01-22T18:53:44+0800",
    comments = "version: 1.2.0.Final, compiler: Eclipse JDT (IDE) 3.12.3.v20170228-1205, environment: Java 1.8.0_31 (Oracle Corporation)"
)
@Component
public class OrderMapperImpl implements OrderMapper {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<Order> toEntity(List<OrderDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Order> list = new ArrayList<Order>( dtoList.size() );
        for ( OrderDTO orderDTO : dtoList ) {
            list.add( toEntity( orderDTO ) );
        }

        return list;
    }

    @Override
    public List<OrderDTO> toDto(List<Order> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrderDTO> list = new ArrayList<OrderDTO>( entityList.size() );
        for ( Order order : entityList ) {
            list.add( toDto( order ) );
        }

        return list;
    }

    @Override
    public OrderDTO toDto(Order order) {
        if ( order == null ) {
            return null;
        }

        OrderDTO orderDTO = new OrderDTO();

        Long id = orderGoodsId( order );
        if ( id != null ) {
            orderDTO.setGoodsId( id );
        }
        orderDTO.setId( order.getId() );
        orderDTO.setName( order.getName() );
        orderDTO.setAppid( order.getAppid() );
        orderDTO.setOpenid( order.getOpenid() );

        return orderDTO;
    }

    @Override
    public Order toEntity(OrderDTO orderDTO) {
        if ( orderDTO == null ) {
            return null;
        }

        Order order = new Order();

        order.setGoods( goodsMapper.fromId( orderDTO.getGoodsId() ) );
        order.setId( orderDTO.getId() );
        order.setName( orderDTO.getName() );
        order.setAppid( orderDTO.getAppid() );
        order.setOpenid( orderDTO.getOpenid() );

        return order;
    }

    private Long orderGoodsId(Order order) {
        if ( order == null ) {
            return null;
        }
        Goods goods = order.getGoods();
        if ( goods == null ) {
            return null;
        }
        Long id = goods.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
