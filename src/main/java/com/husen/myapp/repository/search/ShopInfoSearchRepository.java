package com.husen.myapp.repository.search;

import com.husen.myapp.domain.ShopInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShopInfo entity.
 */
public interface ShopInfoSearchRepository extends ElasticsearchRepository<ShopInfo, Long> {
}
