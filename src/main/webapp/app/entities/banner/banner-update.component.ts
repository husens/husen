import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IBanner } from 'app/shared/model/banner.model';
import { BannerService } from './banner.service';
import { IShop } from 'app/shared/model/shop.model';
import { ShopService } from 'app/entities/shop';

@Component({
    selector: 'jhi-banner-update',
    templateUrl: './banner-update.component.html'
})
export class BannerUpdateComponent implements OnInit {
    banner: IBanner;
    isSaving: boolean;

    shops: IShop[];

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected bannerService: BannerService,
        protected shopService: ShopService,
        protected elementRef: ElementRef,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ banner }) => {
            this.banner = banner;
        });
        this.shopService.query().subscribe(
            (res: HttpResponse<IShop[]>) => {
                this.shops = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.banner, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.banner.id !== undefined) {
            this.subscribeToSaveResponse(this.bannerService.update(this.banner));
        } else {
            this.subscribeToSaveResponse(this.bannerService.create(this.banner));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBanner>>) {
        result.subscribe((res: HttpResponse<IBanner>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackShopById(index: number, item: IShop) {
        return item.id;
    }
}
