export * from './shop-info.service';
export * from './shop-info-update.component';
export * from './shop-info-delete-dialog.component';
export * from './shop-info-detail.component';
export * from './shop-info.component';
export * from './shop-info.route';
