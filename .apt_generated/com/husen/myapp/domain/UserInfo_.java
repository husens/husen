package com.husen.myapp.domain;

import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserInfo.class)
public abstract class UserInfo_ {

	public static volatile SingularAttribute<UserInfo, Instant> vipTime;
	public static volatile SingularAttribute<UserInfo, Long> conSume;
	public static volatile SingularAttribute<UserInfo, Integer> vipSale;
	public static volatile SingularAttribute<UserInfo, String> vipLevelName;
	public static volatile SingularAttribute<UserInfo, Long> id;
	public static volatile SingularAttribute<UserInfo, Integer> vipState;
	public static volatile SingularAttribute<UserInfo, User> user;

}

