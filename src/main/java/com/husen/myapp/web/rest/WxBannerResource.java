package com.husen.myapp.web.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.domain.Banner;
import com.husen.myapp.domain.enumeration.BannerType;
import com.husen.myapp.repository.WxBannerRepository;
import com.husen.myapp.service.BannerService;
import com.husen.myapp.service.mapper.BannerMapper;
import com.husen.myapp.util.ShopConstant;

/**
 * REST controller for managing Banner.
 */
@RestController
@RequestMapping("/api")
public class WxBannerResource {

    private final Logger log = LoggerFactory.getLogger(WxBannerResource.class);

    private static final String ENTITY_NAME = "banner";

    private final BannerService bannerService;

    @Autowired
    private WxBannerRepository wxBannerRepository;
    
    private final BannerMapper bannerMapper;
    
    public WxBannerResource(BannerService bannerService,BannerMapper bannerMapper) {
        this.bannerService = bannerService;
        this.bannerMapper = bannerMapper;
    }
    
    @GetMapping("/banner_list")
    @Timed 
    public ResponseEntity<Object> getbanners(@RequestParam(required=false) String type) {
        log.debug("REST request to get coupons_check : {} userId", type);
        
       List<Banner> banners = new ArrayList<Banner>();
       
        if(ShopConstant.ENTITY_BANNER_TYPE_HOME.equals(type)){
        	banners = wxBannerRepository.findByType(BannerType.HOME);
        	 
        }else if(ShopConstant.ENTITY_BANNER_TYPE_MENU.equals(type)){
        	banners = wxBannerRepository.findByType(BannerType.MENU);
        }       
        
        for(Banner banner:banners) banner.setPic(null);

        Map<String,Object> body = new HashMap<String,Object>();
       	body.put(ShopConstant.RESULT_CODE, ShopConstant.RESULT_CODE_SUCCESS);
       	
    	body.put(ShopConstant.RESULT_DATA, banners!=null?bannerMapper.toDto(banners):null);
    	return ResponseEntity.ok().body(body);
    }
}
