package com.husen.myapp.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the UserInfo entity.
 */
public class UserInfoDTO implements Serializable {

    private Long id;

    private String vipLevelName;

    private Integer vipState;

    private Integer vipSale;

    private Instant vipTime;

    private Long conSume;

    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVipLevelName() {
        return vipLevelName;
    }

    public void setVipLevelName(String vipLevelName) {
        this.vipLevelName = vipLevelName;
    }

    public Integer getVipState() {
        return vipState;
    }

    public void setVipState(Integer vipState) {
        this.vipState = vipState;
    }

    public Integer getVipSale() {
        return vipSale;
    }

    public void setVipSale(Integer vipSale) {
        this.vipSale = vipSale;
    }

    public Instant getVipTime() {
        return vipTime;
    }

    public void setVipTime(Instant vipTime) {
        this.vipTime = vipTime;
    }

    public Long getConSume() {
        return conSume;
    }

    public void setConSume(Long conSume) {
        this.conSume = conSume;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserInfoDTO userInfoDTO = (UserInfoDTO) o;
        if (userInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserInfoDTO{" +
            "id=" + getId() +
            ", vipLevelName='" + getVipLevelName() + "'" +
            ", vipState=" + getVipState() +
            ", vipSale=" + getVipSale() +
            ", vipTime='" + getVipTime() + "'" +
            ", conSume=" + getConSume() +
            ", user=" + getUserId() +
            "}";
    }
}
