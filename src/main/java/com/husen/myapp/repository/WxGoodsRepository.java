package com.husen.myapp.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.husen.myapp.domain.Goods;
import com.husen.myapp.domain.enumeration.GoodsStatus;


/**
 * Spring Data  repository for the Goods entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WxGoodsRepository extends GoodsRepository {
	List<Goods> findByStatus( GoodsStatus status);
}
