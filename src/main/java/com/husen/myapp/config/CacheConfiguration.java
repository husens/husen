package com.husen.myapp.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.husen.myapp.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.husen.myapp.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Goods.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Shop.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.ShopInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.ShopInfo.class.getName() + ".coupons", jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Coupons.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.UserInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Shop.class.getName() + ".goods", jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Shop.class.getName() + ".banners", jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Banner.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Goods.class.getName() + ".orders", jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Order.class.getName(), jcacheConfiguration);
            cm.createCache(com.husen.myapp.domain.Goods.class.getName() + ".order1s", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
