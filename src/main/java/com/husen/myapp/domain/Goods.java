package com.husen.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.husen.myapp.domain.enumeration.GoodsStatus;

/**
 * A Goods.
 */
@Entity
@Table(name = "goods")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "goods")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "appid")
    private String appid;

    @Column(name = "openid")
    private String openid;

    @Column(name = "pic")
    private String pic;

    @Lob
    @Column(name = "pic_img")
    private byte[] picImg;

    @Column(name = "pic_img_content_type")
    private String picImgContentType;

    @Column(name = "mini_price")
    private Long miniPrice;

    @Column(name = "orders")
    private Long orders;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status")
    private GoodsStatus status;

    @OneToMany(mappedBy = "goods")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Order> order1s = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("goods")
    private Shop shop;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Goods name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppid() {
        return appid;
    }

    public Goods appid(String appid) {
        this.appid = appid;
        return this;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getOpenid() {
        return openid;
    }

    public Goods openid(String openid) {
        this.openid = openid;
        return this;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getPic() {
        return pic;
    }

    public Goods pic(String pic) {
        this.pic = pic;
        return this;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public byte[] getPicImg() {
        return picImg;
    }

    public Goods picImg(byte[] picImg) {
        this.picImg = picImg;
        return this;
    }

    public void setPicImg(byte[] picImg) {
        this.picImg = picImg;
    }

    public String getPicImgContentType() {
        return picImgContentType;
    }

    public Goods picImgContentType(String picImgContentType) {
        this.picImgContentType = picImgContentType;
        return this;
    }

    public void setPicImgContentType(String picImgContentType) {
        this.picImgContentType = picImgContentType;
    }

    public Long getMiniPrice() {
        return miniPrice;
    }

    public Goods miniPrice(Long miniPrice) {
        this.miniPrice = miniPrice;
        return this;
    }

    public void setMiniPrice(Long miniPrice) {
        this.miniPrice = miniPrice;
    }

    public Long getOrders() {
        return orders;
    }

    public Goods orders(Long orders) {
        this.orders = orders;
        return this;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    public GoodsStatus getStatus() {
        return status;
    }

    public Goods status(GoodsStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(GoodsStatus status) {
        this.status = status;
    }

    public Set<Order> getOrder1s() {
        return order1s;
    }

    public Goods order1s(Set<Order> orders) {
        this.order1s = orders;
        return this;
    }

    public Goods addOrder1s(Order order) {
        this.order1s.add(order);
        order.setGoods(this);
        return this;
    }

    public Goods removeOrder1s(Order order) {
        this.order1s.remove(order);
        order.setGoods(null);
        return this;
    }

    public void setOrder1s(Set<Order> orders) {
        this.order1s = orders;
    }

    public Shop getShop() {
        return shop;
    }

    public Goods shop(Shop shop) {
        this.shop = shop;
        return this;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Goods goods = (Goods) o;
        if (goods.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), goods.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Goods{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", appid='" + getAppid() + "'" +
            ", openid='" + getOpenid() + "'" +
            ", pic='" + getPic() + "'" +
            ", picImg='" + getPicImg() + "'" +
            ", picImgContentType='" + getPicImgContentType() + "'" +
            ", miniPrice=" + getMiniPrice() +
            ", orders=" + getOrders() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
