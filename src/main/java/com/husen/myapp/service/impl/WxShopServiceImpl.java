package com.husen.myapp.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.husen.myapp.domain.Shop;
import com.husen.myapp.domain.User;
import com.husen.myapp.repository.ShopRepository;
import com.husen.myapp.repository.WxShopRepository;
import com.husen.myapp.repository.search.ShopSearchRepository;
import com.husen.myapp.service.WxShopService;
import com.husen.myapp.service.dto.ShopDTO;
import com.husen.myapp.service.mapper.ShopMapper;

/**
 * Service Implementation for managing Shop.
 */
@Service
@Transactional
public class WxShopServiceImpl  implements WxShopService  {


	private final Logger log = LoggerFactory.getLogger(WxShopServiceImpl.class);

    private final WxShopRepository wxshopRepository;

    public WxShopServiceImpl(WxShopRepository wxshopRepository) {
    	this.wxshopRepository = wxshopRepository;
	}
     
    @Override
    @Transactional(readOnly = true)
    public Optional<Shop> findOneByUser(User user) {
        log.debug("Request to get Shop : {} userId", user.getId());
        return wxshopRepository.findByUser(user); 
    }
}
