/**
 * View Models used by Spring MVC REST controllers.
 */
package com.husen.myapp.web.rest.vm;
