package com.husen.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the ShopInfo entity.
 */
public class ShopInfoDTO implements Serializable {

    private Long id;

    private String sname;

    private String gname;

    private String gadds;

    private Integer homeType;

    private Integer carType;

    private Integer orderType;

    private Integer vipType;

    private Integer vipSale;

    private String spic;

    private String gpic;

    @Lob
    private byte[] spicImg;
    private String spicImgContentType;

    @Lob
    private byte[] gpicImg;
    private String gpicImgContentType;

    private String searchTag;

    private String carText;

    private String times;

    private String phone;

    private Long coupons;

    private Long coupons1Id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getGadds() {
        return gadds;
    }

    public void setGadds(String gadds) {
        this.gadds = gadds;
    }

    public Integer getHomeType() {
        return homeType;
    }

    public void setHomeType(Integer homeType) {
        this.homeType = homeType;
    }

    public Integer getCarType() {
        return carType;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getVipType() {
        return vipType;
    }

    public void setVipType(Integer vipType) {
        this.vipType = vipType;
    }

    public Integer getVipSale() {
        return vipSale;
    }

    public void setVipSale(Integer vipSale) {
        this.vipSale = vipSale;
    }

    public String getSpic() {
        return spic;
    }

    public void setSpic(String spic) {
        this.spic = spic;
    }

    public String getGpic() {
        return gpic;
    }

    public void setGpic(String gpic) {
        this.gpic = gpic;
    }

    public byte[] getSpicImg() {
        return spicImg;
    }

    public void setSpicImg(byte[] spicImg) {
        this.spicImg = spicImg;
    }

    public String getSpicImgContentType() {
        return spicImgContentType;
    }

    public void setSpicImgContentType(String spicImgContentType) {
        this.spicImgContentType = spicImgContentType;
    }

    public byte[] getGpicImg() {
        return gpicImg;
    }

    public void setGpicImg(byte[] gpicImg) {
        this.gpicImg = gpicImg;
    }

    public String getGpicImgContentType() {
        return gpicImgContentType;
    }

    public void setGpicImgContentType(String gpicImgContentType) {
        this.gpicImgContentType = gpicImgContentType;
    }

    public String getSearchTag() {
        return searchTag;
    }

    public void setSearchTag(String searchTag) {
        this.searchTag = searchTag;
    }

    public String getCarText() {
        return carText;
    }

    public void setCarText(String carText) {
        this.carText = carText;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getCoupons() {
        return coupons;
    }

    public void setCoupons(Long coupons) {
        this.coupons = coupons;
    }

    public Long getCoupons1Id() {
        return coupons1Id;
    }

    public void setCoupons1Id(Long couponsId) {
        this.coupons1Id = couponsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShopInfoDTO shopInfoDTO = (ShopInfoDTO) o;
        if (shopInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shopInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShopInfoDTO{" +
            "id=" + getId() +
            ", sname='" + getSname() + "'" +
            ", gname='" + getGname() + "'" +
            ", gadds='" + getGadds() + "'" +
            ", homeType=" + getHomeType() +
            ", carType=" + getCarType() +
            ", orderType=" + getOrderType() +
            ", vipType=" + getVipType() +
            ", vipSale=" + getVipSale() +
            ", spic='" + getSpic() + "'" +
            ", gpic='" + getGpic() + "'" +
            ", spicImg='" + getSpicImg() + "'" +
            ", gpicImg='" + getGpicImg() + "'" +
            ", searchTag='" + getSearchTag() + "'" +
            ", carText='" + getCarText() + "'" +
            ", times='" + getTimes() + "'" +
            ", phone='" + getPhone() + "'" +
            ", coupons=" + getCoupons() +
            ", coupons1=" + getCoupons1Id() +
            "}";
    }
}
