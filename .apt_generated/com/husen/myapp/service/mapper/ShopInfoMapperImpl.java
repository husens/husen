package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.Coupons;
import com.husen.myapp.domain.ShopInfo;
import com.husen.myapp.service.dto.ShopInfoDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-01-22T18:53:44+0800",
    comments = "version: 1.2.0.Final, compiler: Eclipse JDT (IDE) 3.12.3.v20170228-1205, environment: Java 1.8.0_31 (Oracle Corporation)"
)
@Component
public class ShopInfoMapperImpl implements ShopInfoMapper {

    @Autowired
    private CouponsMapper couponsMapper;

    @Override
    public List<ShopInfo> toEntity(List<ShopInfoDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ShopInfo> list = new ArrayList<ShopInfo>( dtoList.size() );
        for ( ShopInfoDTO shopInfoDTO : dtoList ) {
            list.add( toEntity( shopInfoDTO ) );
        }

        return list;
    }

    @Override
    public List<ShopInfoDTO> toDto(List<ShopInfo> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ShopInfoDTO> list = new ArrayList<ShopInfoDTO>( entityList.size() );
        for ( ShopInfo shopInfo : entityList ) {
            list.add( toDto( shopInfo ) );
        }

        return list;
    }

    @Override
    public ShopInfoDTO toDto(ShopInfo shopInfo) {
        if ( shopInfo == null ) {
            return null;
        }

        ShopInfoDTO shopInfoDTO = new ShopInfoDTO();

        Long id = shopInfoCoupons1Id( shopInfo );
        if ( id != null ) {
            shopInfoDTO.setCoupons1Id( id );
        }
        shopInfoDTO.setId( shopInfo.getId() );
        shopInfoDTO.setSname( shopInfo.getSname() );
        shopInfoDTO.setGname( shopInfo.getGname() );
        shopInfoDTO.setGadds( shopInfo.getGadds() );
        shopInfoDTO.setHomeType( shopInfo.getHomeType() );
        shopInfoDTO.setCarType( shopInfo.getCarType() );
        shopInfoDTO.setOrderType( shopInfo.getOrderType() );
        shopInfoDTO.setVipType( shopInfo.getVipType() );
        shopInfoDTO.setVipSale( shopInfo.getVipSale() );
        shopInfoDTO.setSpic( shopInfo.getSpic() );
        shopInfoDTO.setGpic( shopInfo.getGpic() );
        byte[] spicImg = shopInfo.getSpicImg();
        if ( spicImg != null ) {
            shopInfoDTO.setSpicImg( Arrays.copyOf( spicImg, spicImg.length ) );
        }
        shopInfoDTO.setSpicImgContentType( shopInfo.getSpicImgContentType() );
        byte[] gpicImg = shopInfo.getGpicImg();
        if ( gpicImg != null ) {
            shopInfoDTO.setGpicImg( Arrays.copyOf( gpicImg, gpicImg.length ) );
        }
        shopInfoDTO.setGpicImgContentType( shopInfo.getGpicImgContentType() );
        shopInfoDTO.setSearchTag( shopInfo.getSearchTag() );
        shopInfoDTO.setCarText( shopInfo.getCarText() );
        shopInfoDTO.setTimes( shopInfo.getTimes() );
        shopInfoDTO.setPhone( shopInfo.getPhone() );
        shopInfoDTO.setCoupons( shopInfo.getCoupons() );

        return shopInfoDTO;
    }

    @Override
    public ShopInfo toEntity(ShopInfoDTO shopInfoDTO) {
        if ( shopInfoDTO == null ) {
            return null;
        }

        ShopInfo shopInfo = new ShopInfo();

        shopInfo.setCoupons1( couponsMapper.fromId( shopInfoDTO.getCoupons1Id() ) );
        shopInfo.setId( shopInfoDTO.getId() );
        shopInfo.setSname( shopInfoDTO.getSname() );
        shopInfo.setGname( shopInfoDTO.getGname() );
        shopInfo.setGadds( shopInfoDTO.getGadds() );
        shopInfo.setHomeType( shopInfoDTO.getHomeType() );
        shopInfo.setCarType( shopInfoDTO.getCarType() );
        shopInfo.setOrderType( shopInfoDTO.getOrderType() );
        shopInfo.setVipType( shopInfoDTO.getVipType() );
        shopInfo.setVipSale( shopInfoDTO.getVipSale() );
        shopInfo.setSpic( shopInfoDTO.getSpic() );
        shopInfo.setGpic( shopInfoDTO.getGpic() );
        byte[] spicImg = shopInfoDTO.getSpicImg();
        if ( spicImg != null ) {
            shopInfo.setSpicImg( Arrays.copyOf( spicImg, spicImg.length ) );
        }
        shopInfo.setSpicImgContentType( shopInfoDTO.getSpicImgContentType() );
        byte[] gpicImg = shopInfoDTO.getGpicImg();
        if ( gpicImg != null ) {
            shopInfo.setGpicImg( Arrays.copyOf( gpicImg, gpicImg.length ) );
        }
        shopInfo.setGpicImgContentType( shopInfoDTO.getGpicImgContentType() );
        shopInfo.setSearchTag( shopInfoDTO.getSearchTag() );
        shopInfo.setCarText( shopInfoDTO.getCarText() );
        shopInfo.setTimes( shopInfoDTO.getTimes() );
        shopInfo.setPhone( shopInfoDTO.getPhone() );
        shopInfo.setCoupons( shopInfoDTO.getCoupons() );

        return shopInfo;
    }

    private Long shopInfoCoupons1Id(ShopInfo shopInfo) {
        if ( shopInfo == null ) {
            return null;
        }
        Coupons coupons1 = shopInfo.getCoupons1();
        if ( coupons1 == null ) {
            return null;
        }
        Long id = coupons1.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
