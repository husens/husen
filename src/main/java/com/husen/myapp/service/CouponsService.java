package com.husen.myapp.service;

import com.husen.myapp.service.dto.CouponsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Coupons.
 */
public interface CouponsService {

    /**
     * Save a coupons.
     *
     * @param couponsDTO the entity to save
     * @return the persisted entity
     */
    CouponsDTO save(CouponsDTO couponsDTO);

    /**
     * Get all the coupons.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<CouponsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" coupons.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CouponsDTO> findOne(Long id);

    /**
     * Delete the "id" coupons.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the coupons corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<CouponsDTO> search(String query, Pageable pageable);
}
