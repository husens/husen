package com.husen.myapp.domain.enumeration;

/**
 * The GoodsStatus enumeration.
 */
public enum GoodsStatus {
    STATUSA, STATUSB
}
