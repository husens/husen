package com.husen.myapp.repository.search;

import com.husen.myapp.domain.Coupons;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Coupons entity.
 */
public interface CouponsSearchRepository extends ElasticsearchRepository<Coupons, Long> {
}
