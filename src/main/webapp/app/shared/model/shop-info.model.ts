export interface IShopInfo {
    id?: number;
    sname?: string;
    gname?: string;
    gadds?: string;
    homeType?: number;
    carType?: number;
    orderType?: number;
    vipType?: number;
    vipSale?: number;
    spic?: string;
    gpic?: string;
    spicImgContentType?: string;
    spicImg?: any;
    gpicImgContentType?: string;
    gpicImg?: any;
    searchTag?: string;
    carText?: string;
    times?: string;
    phone?: string;
    coupons?: number;
    coupons1Id?: number;
    shopId?: number;
}

export class ShopInfo implements IShopInfo {
    constructor(
        public id?: number,
        public sname?: string,
        public gname?: string,
        public gadds?: string,
        public homeType?: number,
        public carType?: number,
        public orderType?: number,
        public vipType?: number,
        public vipSale?: number,
        public spic?: string,
        public gpic?: string,
        public spicImgContentType?: string,
        public spicImg?: any,
        public gpicImgContentType?: string,
        public gpicImg?: any,
        public searchTag?: string,
        public carText?: string,
        public times?: string,
        public phone?: string,
        public coupons?: number,
        public coupons1Id?: number,
        public shopId?: number
    ) {}
}
