import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LoginModalService, AccountService, Account } from 'app/core';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    chartOption: any;
    baroptions: any;
    linkoption: any;
    data: any[];
    loveData: any[];
    showloading: boolean = true;
    constructor(
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.accountService.identity().then(account => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
         
         setTimeout(() => {
	      this.showloading = false;
	    }, 3000);
         
         
         // 堆叠区域图
		 this.chartOption = {
		    title: {
		      text: '堆叠区域图'
		    },
		    tooltip: {
		      trigger: 'axis'
		    },
		    legend: {
		      data: ['微视', '最右', '火山', '快手', '抖音']
		    },
		    toolbox: {
		      feature: {
		        saveAsImage: {}
		      }
		    },
		    grid: {
		      left: '3%',
		      right: '4%',
		      bottom: '3%',
		      containLabel: true
		    },
		    xAxis: [
		      {
		        type: 'category',
		        boundaryGap: false,
		        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
		      },
		    ],
		    yAxis: [
		      {
		        type: 'value'
		      }
		    ],
		    series: [
		      {
		        name: '微视',
		        type: 'line',
		        stack: '总量',
		        areaStyle: {normal: {}},
		        data: [120, 132, 101, 134, 90, 230, 210]
		      },
		      {
		        name: '最右',
		        type: 'line',
		        stack: '总量',
		        areaStyle: {normal: {}},
		        data: [220, 182, 191, 234, 290, 330, 310]
		      },
		      {
		        name: '火山',
		        type: 'line',
		        stack: '总量',
		        areaStyle: {normal: {}},
		        data: [150, 232, 201, 154, 190, 330, 410]
		      },
		      {
		        name: '快手',
		        type: 'line',
		        stack: '总量',
		        areaStyle: {normal: {}},
		        data: [320, 332, 301, 334, 390, 330, 320]
		      },
		      {
		        name: '抖音',
		        type: 'line',
		        stack: '总量',
		        label: {
		          normal: {
		            show: true,
		            position: 'top'
		          }
		        },
		        areaStyle: {normal: {}},
		        data: [820, 930, 901, 934, 1290, 1330, 1320]
		      }
		    ]
		  };
          // 扇形图
		  this.baroptions = {
		    title: {
		      text: '扇形图'
		    },
		    tooltip: {
		      trigger: 'item',
		      formatter: '{a} <br/>{b}: {c} ({d}%)'
		    },
		    legend: {
		      orient: 'vertical',
		      x: 'left',
		      y: '60px',
		      data: ['直达', '营销广告', '搜索引擎', '邮件营销', '联盟广告', '视频广告', '百度', '谷歌', '必应', '其他']
		    },
		    series: [
		      {
		        name: '访问来源',
		        type: 'pie',
		        selectedMode: 'single',
		        radius: [0, '30%'],
		
		        label: {
		          normal: {
		            position: 'inner'
		          }
		        },
		        labelLine: {
		          normal: {
		            show: false
		          }
		        },
		        data: [
		          {value: 335, name: '直达', selected: true},
		          {value: 679, name: '营销广告'},
		          {value: 1548, name: '搜索引擎'}
		        ]
		      },
		      {
		        name: '访问来源',
		        type: 'pie',
		        radius: ['40%', '55%'],
		
		        data: [
		          {value: 335, name: '直达'},
		          {value: 310, name: '短信'},
		          {value: 234, name: '广告联盟'},
		          {value: 135, name: '视频'},
		          {value: 1048, name: '百度'},
		          {value: 251, name: '谷歌'},
		          {value: 147, name: '搜狗'},
		          {value: 102, name: '360'}
		        ]
		      }
		    ]
		  };
		  // 蓝猫今日访问量
		  this.linkoption = {
		    title: {
		      text: '懒猫今日访问量'
		    },
		    color: ['#D48265'],
		    // 气泡提示框，常用于展现更详细的数据
		    tooltip: {
		      trigger: 'axis',
		      axisPointer: { // 坐标轴指示器，坐标轴触发有效
		        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
		      }
		    },
		    toolbox: {
		      show: true,
		      feature: {
		        // 显示缩放按钮
		        dataZoom: {
		          show: true
		        },
		        // 显示折线和块状图之间的切换
		        magicType: {
		          show: true,
		          type: ['bar', 'line']
		        },
		        // 显示是否还原
		        restore: {
		          show: true
		        },
		        // 是否显示图片
		        saveAsImage: {
		          show: true
		        }
		      }
		    },
		    grid: {
		      left: '3%',
		      right: '4%',
		      bottom: '3%',
		      containLabel: true
		    },
		    xAxis: [{
		      name: '类别',
		      type: 'category',
		      data: [2231, 1212, 1231, 3213, 1234, 2666, 6785],
		      axisTick: {
		        alignWithLabel: true
		      },
		      axisLabel: {
		        interval: 0,
		        rotate: 20
		      },
		    }],
		    yAxis: [{
		      name: '访问量',
		      type: 'value'
		    }],
		    series: [{
		      name: '今日访问次数',
		      type: 'bar',
		      barWidth: '60%',
		      label: {
		        normal: {
		          show: true
		        }
		      },
		      data: [2231, 1212, 1231, 3213, 1234, 2666, 6785]
		    }]
		  };
		 
		  
    
    }
	 
    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.accountService.identity().then(account => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
	
	
}
