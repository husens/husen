package com.husen.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Shop.
 */
@Entity
@Table(name = "shop")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shop")
public class Shop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne    @JoinColumn(unique = true)
    private User user;

    @OneToOne    @JoinColumn(unique = true)
    private ShopInfo shopInfo;

    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Goods> goods = new HashSet<>();
    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Banner> banners = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Shop name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public Shop user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ShopInfo getShopInfo() {
        return shopInfo;
    }

    public Shop shopInfo(ShopInfo shopInfo) {
        this.shopInfo = shopInfo;
        return this;
    }

    public void setShopInfo(ShopInfo shopInfo) {
        this.shopInfo = shopInfo;
    }

    public Set<Goods> getGoods() {
        return goods;
    }

    public Shop goods(Set<Goods> goods) {
        this.goods = goods;
        return this;
    }

    public Shop addGoods(Goods goods) {
        this.goods.add(goods);
        goods.setShop(this);
        return this;
    }

    public Shop removeGoods(Goods goods) {
        this.goods.remove(goods);
        goods.setShop(null);
        return this;
    }

    public void setGoods(Set<Goods> goods) {
        this.goods = goods;
    }

    public Set<Banner> getBanners() {
        return banners;
    }

    public Shop banners(Set<Banner> banners) {
        this.banners = banners;
        return this;
    }

    public Shop addBanners(Banner banner) {
        this.banners.add(banner);
        banner.setShop(this);
        return this;
    }

    public Shop removeBanners(Banner banner) {
        this.banners.remove(banner);
        banner.setShop(null);
        return this;
    }

    public void setBanners(Set<Banner> banners) {
        this.banners = banners;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Shop shop = (Shop) o;
        if (shop.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shop.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Shop{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
