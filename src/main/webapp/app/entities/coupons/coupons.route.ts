import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Coupons } from 'app/shared/model/coupons.model';
import { CouponsService } from './coupons.service';
import { CouponsComponent } from './coupons.component';
import { CouponsDetailComponent } from './coupons-detail.component';
import { CouponsUpdateComponent } from './coupons-update.component';
import { CouponsDeletePopupComponent } from './coupons-delete-dialog.component';
import { ICoupons } from 'app/shared/model/coupons.model';

@Injectable({ providedIn: 'root' })
export class CouponsResolve implements Resolve<ICoupons> {
    constructor(private service: CouponsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Coupons> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Coupons>) => response.ok),
                map((coupons: HttpResponse<Coupons>) => coupons.body)
            );
        }
        return of(new Coupons());
    }
}

export const couponsRoute: Routes = [
    {
        path: 'coupons',
        component: CouponsComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'husenApp.coupons.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'coupons/:id/view',
        component: CouponsDetailComponent,
        resolve: {
            coupons: CouponsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'husenApp.coupons.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'coupons/new',
        component: CouponsUpdateComponent,
        resolve: {
            coupons: CouponsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'husenApp.coupons.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'coupons/:id/edit',
        component: CouponsUpdateComponent,
        resolve: {
            coupons: CouponsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'husenApp.coupons.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const couponsPopupRoute: Routes = [
    {
        path: 'coupons/:id/delete',
        component: CouponsDeletePopupComponent,
        resolve: {
            coupons: CouponsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'husenApp.coupons.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
