package com.husen.myapp.service.impl;

import com.husen.myapp.service.CouponsService;
import com.husen.myapp.domain.Coupons;
import com.husen.myapp.repository.CouponsRepository;
import com.husen.myapp.repository.search.CouponsSearchRepository;
import com.husen.myapp.service.dto.CouponsDTO;
import com.husen.myapp.service.mapper.CouponsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Coupons.
 */
@Service
@Transactional
public class CouponsServiceImpl implements CouponsService {

    private final Logger log = LoggerFactory.getLogger(CouponsServiceImpl.class);

    private final CouponsRepository couponsRepository;

    private final CouponsMapper couponsMapper;

    private final CouponsSearchRepository couponsSearchRepository;

    public CouponsServiceImpl(CouponsRepository couponsRepository, CouponsMapper couponsMapper, CouponsSearchRepository couponsSearchRepository) {
        this.couponsRepository = couponsRepository;
        this.couponsMapper = couponsMapper;
        this.couponsSearchRepository = couponsSearchRepository;
    }

    /**
     * Save a coupons.
     *
     * @param couponsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CouponsDTO save(CouponsDTO couponsDTO) {
        log.debug("Request to save Coupons : {}", couponsDTO);

        Coupons coupons = couponsMapper.toEntity(couponsDTO);
        coupons = couponsRepository.save(coupons);
        CouponsDTO result = couponsMapper.toDto(coupons);
        couponsSearchRepository.save(coupons);
        return result;
    }

    /**
     * Get all the coupons.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CouponsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Coupons");
        return couponsRepository.findAll(pageable)
            .map(couponsMapper::toDto);
    }


    /**
     * Get one coupons by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CouponsDTO> findOne(Long id) {
        log.debug("Request to get Coupons : {}", id);
        return couponsRepository.findById(id)
            .map(couponsMapper::toDto);
    }

    /**
     * Delete the coupons by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Coupons : {}", id);
        couponsRepository.deleteById(id);
        couponsSearchRepository.deleteById(id);
    }

    /**
     * Search for the coupons corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CouponsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Coupons for query {}", query);
        return couponsSearchRepository.search(queryStringQuery(query), pageable)
            .map(couponsMapper::toDto);
    }
}
