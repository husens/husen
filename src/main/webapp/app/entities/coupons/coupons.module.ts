import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HusenSharedModule } from 'app/shared';
import {
    CouponsComponent,
    CouponsDetailComponent,
    CouponsUpdateComponent,
    CouponsDeletePopupComponent,
    CouponsDeleteDialogComponent,
    couponsRoute,
    couponsPopupRoute
} from './';

const ENTITY_STATES = [...couponsRoute, ...couponsPopupRoute];

@NgModule({
    imports: [HusenSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CouponsComponent,
        CouponsDetailComponent,
        CouponsUpdateComponent,
        CouponsDeleteDialogComponent,
        CouponsDeletePopupComponent
    ],
    entryComponents: [CouponsComponent, CouponsUpdateComponent, CouponsDeleteDialogComponent, CouponsDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HusenCouponsModule {}
