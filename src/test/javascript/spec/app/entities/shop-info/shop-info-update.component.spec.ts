/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { HusenTestModule } from '../../../test.module';
import { ShopInfoUpdateComponent } from 'app/entities/shop-info/shop-info-update.component';
import { ShopInfoService } from 'app/entities/shop-info/shop-info.service';
import { ShopInfo } from 'app/shared/model/shop-info.model';

describe('Component Tests', () => {
    describe('ShopInfo Management Update Component', () => {
        let comp: ShopInfoUpdateComponent;
        let fixture: ComponentFixture<ShopInfoUpdateComponent>;
        let service: ShopInfoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HusenTestModule],
                declarations: [ShopInfoUpdateComponent]
            })
                .overrideTemplate(ShopInfoUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ShopInfoUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ShopInfoService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ShopInfo(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.shopInfo = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ShopInfo();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.shopInfo = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
