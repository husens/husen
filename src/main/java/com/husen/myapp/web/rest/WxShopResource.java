package com.husen.myapp.web.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.domain.Shop;
import com.husen.myapp.domain.User;
import com.husen.myapp.service.ShopService;
import com.husen.myapp.service.UserService;
import com.husen.myapp.service.WxShopService;
import com.husen.myapp.service.dto.ShopDTO;
import com.husen.myapp.service.dto.ShopInfoDTO;
import com.husen.myapp.service.mapper.ShopInfoMapper;
import com.husen.myapp.service.mapper.ShopMapper;
import com.husen.myapp.util.ShopConstant;

/**
 * REST controller for managing Shop.
 */
@RestController
@RequestMapping("/api")
public class WxShopResource {

    private final Logger log = LoggerFactory.getLogger(WxShopResource.class);

    private static final String ENTITY_NAME = "shop";

    private final ShopService shopService;
    
    private final WxShopService wxshopService;
    
    @Autowired
    private UserService userService;
    
    private final ShopMapper shopMapper;
    private final ShopInfoMapper shopInfoMapper;
    
    public WxShopResource(ShopService shopService,WxShopService wxshopService,ShopMapper shopMapper,ShopInfoMapper shopInfoMapper) {
        this.wxshopService = wxshopService;
        this.shopService = shopService;
        this.shopMapper = shopMapper;
        this.shopInfoMapper = shopInfoMapper;
    }
 
    @GetMapping("/shopinfo")
    @Timed 
    public ResponseEntity<Map> getShopinfo(@RequestParam Long userId,@RequestParam String appid) {
        log.debug("REST request to get Shop : {} userId", userId);
        
        Optional<User> user = userService.getUserWithAuthorities(userId);
        
        Optional<Shop> shop = wxshopService.findOneByUser(user.get());
        shop.get().getShopInfo().setGpicImg(null);
        shop.get().getShopInfo().setSpicImg(null);
        if(shop.get().getShopInfo().getCoupons() == null) shop.get().getShopInfo().setCoupons(new Long(0));
         
        ShopDTO shopr = shopMapper.toDto(shop.get());
         
        
        ShopInfoDTO shopInfor = shopInfoMapper.toDto(shop.get().getShopInfo());
        
        shopInfor.setGpicImg(null);
        shopInfor.setSpicImg(null);
        
    	Map<String,Object> body = new HashMap<String,Object>();
    	
     
    	JSONObject jsonShop = JSON.parseObject(JSON.toJSONString(shopr));
    	JSONObject jsonShopInfo = JSON.parseObject(JSON.toJSONString(shopInfor));
    	jsonShop.put("shopInfo", jsonShopInfo);
    	  
    	body.put(ShopConstant.RESULT_CODE, ShopConstant.RESULT_CODE_SUCCESS);
    	
    	body.put(ShopConstant.RESULT_DATA, jsonShop);
    	
    	return ResponseEntity.ok().body(body);
    }
}
