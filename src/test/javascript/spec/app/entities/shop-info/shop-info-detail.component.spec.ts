/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HusenTestModule } from '../../../test.module';
import { ShopInfoDetailComponent } from 'app/entities/shop-info/shop-info-detail.component';
import { ShopInfo } from 'app/shared/model/shop-info.model';

describe('Component Tests', () => {
    describe('ShopInfo Management Detail Component', () => {
        let comp: ShopInfoDetailComponent;
        let fixture: ComponentFixture<ShopInfoDetailComponent>;
        const route = ({ data: of({ shopInfo: new ShopInfo(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HusenTestModule],
                declarations: [ShopInfoDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ShopInfoDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ShopInfoDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.shopInfo).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
