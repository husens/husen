package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.User;
import com.husen.myapp.domain.UserInfo;
import com.husen.myapp.service.dto.UserInfoDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-01-22T18:53:44+0800",
    comments = "version: 1.2.0.Final, compiler: Eclipse JDT (IDE) 3.12.3.v20170228-1205, environment: Java 1.8.0_31 (Oracle Corporation)"
)
@Component
public class UserInfoMapperImpl implements UserInfoMapper {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserInfo> toEntity(List<UserInfoDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserInfo> list = new ArrayList<UserInfo>( dtoList.size() );
        for ( UserInfoDTO userInfoDTO : dtoList ) {
            list.add( toEntity( userInfoDTO ) );
        }

        return list;
    }

    @Override
    public List<UserInfoDTO> toDto(List<UserInfo> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserInfoDTO> list = new ArrayList<UserInfoDTO>( entityList.size() );
        for ( UserInfo userInfo : entityList ) {
            list.add( toDto( userInfo ) );
        }

        return list;
    }

    @Override
    public UserInfoDTO toDto(UserInfo userInfo) {
        if ( userInfo == null ) {
            return null;
        }

        UserInfoDTO userInfoDTO = new UserInfoDTO();

        Long id = userInfoUserId( userInfo );
        if ( id != null ) {
            userInfoDTO.setUserId( id );
        }
        userInfoDTO.setId( userInfo.getId() );
        userInfoDTO.setVipLevelName( userInfo.getVipLevelName() );
        userInfoDTO.setVipState( userInfo.getVipState() );
        userInfoDTO.setVipSale( userInfo.getVipSale() );
        userInfoDTO.setVipTime( userInfo.getVipTime() );
        userInfoDTO.setConSume( userInfo.getConSume() );

        return userInfoDTO;
    }

    @Override
    public UserInfo toEntity(UserInfoDTO userInfoDTO) {
        if ( userInfoDTO == null ) {
            return null;
        }

        UserInfo userInfo = new UserInfo();

        userInfo.setUser( userMapper.userFromId( userInfoDTO.getUserId() ) );
        userInfo.setId( userInfoDTO.getId() );
        userInfo.setVipLevelName( userInfoDTO.getVipLevelName() );
        userInfo.setVipState( userInfoDTO.getVipState() );
        userInfo.setVipSale( userInfoDTO.getVipSale() );
        userInfo.setVipTime( userInfoDTO.getVipTime() );
        userInfo.setConSume( userInfoDTO.getConSume() );

        return userInfo;
    }

    private Long userInfoUserId(UserInfo userInfo) {
        if ( userInfo == null ) {
            return null;
        }
        User user = userInfo.getUser();
        if ( user == null ) {
            return null;
        }
        Long id = user.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
