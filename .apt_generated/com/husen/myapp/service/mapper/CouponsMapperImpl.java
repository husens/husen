package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.Coupons;
import com.husen.myapp.service.dto.CouponsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-01-22T18:53:44+0800",
    comments = "version: 1.2.0.Final, compiler: Eclipse JDT (IDE) 3.12.3.v20170228-1205, environment: Java 1.8.0_31 (Oracle Corporation)"
)
@Component
public class CouponsMapperImpl implements CouponsMapper {

    @Override
    public Coupons toEntity(CouponsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Coupons coupons = new Coupons();

        coupons.setId( dto.getId() );
        coupons.setName( dto.getName() );

        return coupons;
    }

    @Override
    public CouponsDTO toDto(Coupons entity) {
        if ( entity == null ) {
            return null;
        }

        CouponsDTO couponsDTO = new CouponsDTO();

        couponsDTO.setId( entity.getId() );
        couponsDTO.setName( entity.getName() );

        return couponsDTO;
    }

    @Override
    public List<Coupons> toEntity(List<CouponsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Coupons> list = new ArrayList<Coupons>( dtoList.size() );
        for ( CouponsDTO couponsDTO : dtoList ) {
            list.add( toEntity( couponsDTO ) );
        }

        return list;
    }

    @Override
    public List<CouponsDTO> toDto(List<Coupons> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CouponsDTO> list = new ArrayList<CouponsDTO>( entityList.size() );
        for ( Coupons coupons : entityList ) {
            list.add( toDto( coupons ) );
        }

        return list;
    }
}
