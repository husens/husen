# husen

1，This application was generated using Spring boot,Bootstrap 3,Angular 6,JHipster 5.7.2. 

2，技术交流
   QQ群：729201346
   
# Demo 演示

[微信小程序demo]()

[后台demo(用户名：admin,密码:admin)](http://www.zhuantsoft.com:18080)

![输入图片说明](http://www.zhuantsoft.com:18080/docs/husen/one.png "屏幕截图.png")
![输入图片说明](http://www.zhuantsoft.com:18080/docs/husen/two.png "屏幕截图.png")
![输入图片说明](http://www.zhuantsoft.com:18080/docs/husen/three.png "屏幕截图.png")
![输入图片说明](http://www.zhuantsoft.com:18080/docs/husen/four.png "屏幕截图.png")
![输入图片说明](http://www.zhuantsoft.com:18080/docs/husen/five.png "屏幕截图.png")
![输入图片说明](http://www.zhuantsoft.com:18080/docs/husen/six.png "屏幕截图.png")
 
  
## Development

1，Install Java 8 from the Oracle website. 
    安装JDK 1.8

2，Install Node.js from the Node.js website (please use an LTS 64-bit version, non-LTS versions are not supported) 
     安装Node.js
     

3，NPM is installed with Node.js but you need to upgrade it: npm install -g npm 
     升级 Node.js npm install -g npm

4，If you want to use the JHipster Marketplace, install Yeoman: npm install -g yo, 
     安装 Yeoman npm install -g yo

5，Install JHipster: npm install -g generator-jhipster 
     安装 JHipster npm install -g generator-jhipster

6，首先安装字体依赖文件
   npm install --save font-awesome

7，首先安装 jquery
   npm install jquery --save
