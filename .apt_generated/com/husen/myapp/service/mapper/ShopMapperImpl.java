package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.Shop;
import com.husen.myapp.domain.ShopInfo;
import com.husen.myapp.domain.User;
import com.husen.myapp.service.dto.ShopDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-01-22T18:53:44+0800",
    comments = "version: 1.2.0.Final, compiler: Eclipse JDT (IDE) 3.12.3.v20170228-1205, environment: Java 1.8.0_31 (Oracle Corporation)"
)
@Component
public class ShopMapperImpl implements ShopMapper {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ShopInfoMapper shopInfoMapper;

    @Override
    public List<Shop> toEntity(List<ShopDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Shop> list = new ArrayList<Shop>( dtoList.size() );
        for ( ShopDTO shopDTO : dtoList ) {
            list.add( toEntity( shopDTO ) );
        }

        return list;
    }

    @Override
    public List<ShopDTO> toDto(List<Shop> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ShopDTO> list = new ArrayList<ShopDTO>( entityList.size() );
        for ( Shop shop : entityList ) {
            list.add( toDto( shop ) );
        }

        return list;
    }

    @Override
    public ShopDTO toDto(Shop shop) {
        if ( shop == null ) {
            return null;
        }

        ShopDTO shopDTO = new ShopDTO();

        Long id = shopShopInfoId( shop );
        if ( id != null ) {
            shopDTO.setShopInfoId( id );
        }
        Long id1 = shopUserId( shop );
        if ( id1 != null ) {
            shopDTO.setUserId( id1 );
        }
        shopDTO.setId( shop.getId() );
        shopDTO.setName( shop.getName() );

        return shopDTO;
    }

    @Override
    public Shop toEntity(ShopDTO shopDTO) {
        if ( shopDTO == null ) {
            return null;
        }

        Shop shop = new Shop();

        shop.setShopInfo( shopInfoMapper.fromId( shopDTO.getShopInfoId() ) );
        shop.setUser( userMapper.userFromId( shopDTO.getUserId() ) );
        shop.setId( shopDTO.getId() );
        shop.setName( shopDTO.getName() );

        return shop;
    }

    private Long shopShopInfoId(Shop shop) {
        if ( shop == null ) {
            return null;
        }
        ShopInfo shopInfo = shop.getShopInfo();
        if ( shopInfo == null ) {
            return null;
        }
        Long id = shopInfo.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long shopUserId(Shop shop) {
        if ( shop == null ) {
            return null;
        }
        User user = shop.getUser();
        if ( user == null ) {
            return null;
        }
        Long id = user.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
