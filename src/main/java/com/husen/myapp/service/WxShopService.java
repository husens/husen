package com.husen.myapp.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.husen.myapp.domain.Shop;
import com.husen.myapp.domain.User;
import com.husen.myapp.service.dto.ShopDTO;

/**
 * Service Interface for managing Shop.
 */
public interface WxShopService {

    
    public Optional<Shop> findOneByUser(User user);
}
