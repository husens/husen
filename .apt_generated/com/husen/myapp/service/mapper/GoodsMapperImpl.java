package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.Goods;
import com.husen.myapp.domain.Shop;
import com.husen.myapp.service.dto.GoodsDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-01-22T18:53:44+0800",
    comments = "version: 1.2.0.Final, compiler: Eclipse JDT (IDE) 3.12.3.v20170228-1205, environment: Java 1.8.0_31 (Oracle Corporation)"
)
@Component
public class GoodsMapperImpl implements GoodsMapper {

    @Autowired
    private ShopMapper shopMapper;

    @Override
    public List<Goods> toEntity(List<GoodsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Goods> list = new ArrayList<Goods>( dtoList.size() );
        for ( GoodsDTO goodsDTO : dtoList ) {
            list.add( toEntity( goodsDTO ) );
        }

        return list;
    }

    @Override
    public List<GoodsDTO> toDto(List<Goods> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<GoodsDTO> list = new ArrayList<GoodsDTO>( entityList.size() );
        for ( Goods goods : entityList ) {
            list.add( toDto( goods ) );
        }

        return list;
    }

    @Override
    public GoodsDTO toDto(Goods goods) {
        if ( goods == null ) {
            return null;
        }

        GoodsDTO goodsDTO = new GoodsDTO();

        Long id = goodsShopId( goods );
        if ( id != null ) {
            goodsDTO.setShopId( id );
        }
        goodsDTO.setId( goods.getId() );
        goodsDTO.setName( goods.getName() );
        goodsDTO.setAppid( goods.getAppid() );
        goodsDTO.setOpenid( goods.getOpenid() );
        goodsDTO.setPic( goods.getPic() );
        byte[] picImg = goods.getPicImg();
        if ( picImg != null ) {
            goodsDTO.setPicImg( Arrays.copyOf( picImg, picImg.length ) );
        }
        goodsDTO.setPicImgContentType( goods.getPicImgContentType() );
        goodsDTO.setMiniPrice( goods.getMiniPrice() );
        goodsDTO.setOrders( goods.getOrders() );
        goodsDTO.setStatus( goods.getStatus() );

        return goodsDTO;
    }

    @Override
    public Goods toEntity(GoodsDTO goodsDTO) {
        if ( goodsDTO == null ) {
            return null;
        }

        Goods goods = new Goods();

        goods.setShop( shopMapper.fromId( goodsDTO.getShopId() ) );
        goods.setId( goodsDTO.getId() );
        goods.setName( goodsDTO.getName() );
        goods.setAppid( goodsDTO.getAppid() );
        goods.setOpenid( goodsDTO.getOpenid() );
        goods.setPic( goodsDTO.getPic() );
        byte[] picImg = goodsDTO.getPicImg();
        if ( picImg != null ) {
            goods.setPicImg( Arrays.copyOf( picImg, picImg.length ) );
        }
        goods.setPicImgContentType( goodsDTO.getPicImgContentType() );
        goods.setMiniPrice( goodsDTO.getMiniPrice() );
        goods.setOrders( goodsDTO.getOrders() );
        goods.setStatus( goodsDTO.getStatus() );

        return goods;
    }

    private Long goodsShopId(Goods goods) {
        if ( goods == null ) {
            return null;
        }
        Shop shop = goods.getShop();
        if ( shop == null ) {
            return null;
        }
        Long id = shop.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
