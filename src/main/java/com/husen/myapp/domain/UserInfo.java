package com.husen.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A UserInfo.
 */
@Entity
@Table(name = "user_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "userinfo")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "vip_level_name")
    private String vipLevelName;

    @Column(name = "vip_state")
    private Integer vipState;

    @Column(name = "vip_sale")
    private Integer vipSale;

    @Column(name = "vip_time")
    private Instant vipTime;

    @Column(name = "con_sume")
    private Long conSume;

    @OneToOne    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVipLevelName() {
        return vipLevelName;
    }

    public UserInfo vipLevelName(String vipLevelName) {
        this.vipLevelName = vipLevelName;
        return this;
    }

    public void setVipLevelName(String vipLevelName) {
        this.vipLevelName = vipLevelName;
    }

    public Integer getVipState() {
        return vipState;
    }

    public UserInfo vipState(Integer vipState) {
        this.vipState = vipState;
        return this;
    }

    public void setVipState(Integer vipState) {
        this.vipState = vipState;
    }

    public Integer getVipSale() {
        return vipSale;
    }

    public UserInfo vipSale(Integer vipSale) {
        this.vipSale = vipSale;
        return this;
    }

    public void setVipSale(Integer vipSale) {
        this.vipSale = vipSale;
    }

    public Instant getVipTime() {
        return vipTime;
    }

    public UserInfo vipTime(Instant vipTime) {
        this.vipTime = vipTime;
        return this;
    }

    public void setVipTime(Instant vipTime) {
        this.vipTime = vipTime;
    }

    public Long getConSume() {
        return conSume;
    }

    public UserInfo conSume(Long conSume) {
        this.conSume = conSume;
        return this;
    }

    public void setConSume(Long conSume) {
        this.conSume = conSume;
    }

    public User getUser() {
        return user;
    }

    public UserInfo user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserInfo userInfo = (UserInfo) o;
        if (userInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserInfo{" +
            "id=" + getId() +
            ", vipLevelName='" + getVipLevelName() + "'" +
            ", vipState=" + getVipState() +
            ", vipSale=" + getVipSale() +
            ", vipTime='" + getVipTime() + "'" +
            ", conSume=" + getConSume() +
            "}";
    }
}
