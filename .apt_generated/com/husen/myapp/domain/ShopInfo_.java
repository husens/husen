package com.husen.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ShopInfo.class)
public abstract class ShopInfo_ {

	public static volatile SingularAttribute<ShopInfo, String> gadds;
	public static volatile SingularAttribute<ShopInfo, Integer> orderType;
	public static volatile SingularAttribute<ShopInfo, String> spic;
	public static volatile SingularAttribute<ShopInfo, Shop> shop;
	public static volatile SingularAttribute<ShopInfo, String> spicImgContentType;
	public static volatile SingularAttribute<ShopInfo, Integer> vipType;
	public static volatile SingularAttribute<ShopInfo, Coupons> coupons1;
	public static volatile SingularAttribute<ShopInfo, String> gname;
	public static volatile SingularAttribute<ShopInfo, byte[]> spicImg;
	public static volatile SingularAttribute<ShopInfo, String> searchTag;
	public static volatile SingularAttribute<ShopInfo, String> times;
	public static volatile SingularAttribute<ShopInfo, Integer> carType;
	public static volatile SingularAttribute<ShopInfo, String> gpic;
	public static volatile SingularAttribute<ShopInfo, String> phone;
	public static volatile SingularAttribute<ShopInfo, Long> coupons;
	public static volatile SingularAttribute<ShopInfo, String> sname;
	public static volatile SingularAttribute<ShopInfo, byte[]> gpicImg;
	public static volatile SingularAttribute<ShopInfo, Integer> vipSale;
	public static volatile SingularAttribute<ShopInfo, String> gpicImgContentType;
	public static volatile SingularAttribute<ShopInfo, Long> id;
	public static volatile SingularAttribute<ShopInfo, Integer> homeType;
	public static volatile SingularAttribute<ShopInfo, String> carText;

}

