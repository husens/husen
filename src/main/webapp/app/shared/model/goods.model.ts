import { IOrder } from 'app/shared/model//order.model';

export const enum GoodsStatus {
    STATUSA = 'STATUSA',
    STATUSB = 'STATUSB'
}

export interface IGoods {
    id?: number;
    name?: string;
    appid?: string;
    openid?: string;
    pic?: string;
    picImgContentType?: string;
    picImg?: any;
    miniPrice?: number;
    orders?: number;
    status?: GoodsStatus;
    order1s?: IOrder[];
    shopId?: number;
}

export class Goods implements IGoods {
    constructor(
        public id?: number,
        public name?: string,
        public appid?: string,
        public openid?: string,
        public pic?: string,
        public picImgContentType?: string,
        public picImg?: any,
        public miniPrice?: number,
        public orders?: number,
        public status?: GoodsStatus,
        public order1s?: IOrder[],
        public shopId?: number
    ) {}
}
