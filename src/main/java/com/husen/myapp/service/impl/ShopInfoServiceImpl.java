package com.husen.myapp.service.impl;

import com.husen.myapp.service.ShopInfoService;
import com.husen.myapp.domain.ShopInfo;
import com.husen.myapp.repository.ShopInfoRepository;
import com.husen.myapp.repository.search.ShopInfoSearchRepository;
import com.husen.myapp.service.dto.ShopInfoDTO;
import com.husen.myapp.service.mapper.ShopInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShopInfo.
 */
@Service
@Transactional
public class ShopInfoServiceImpl implements ShopInfoService {

    private final Logger log = LoggerFactory.getLogger(ShopInfoServiceImpl.class);

    private final ShopInfoRepository shopInfoRepository;

    private final ShopInfoMapper shopInfoMapper;

    private final ShopInfoSearchRepository shopInfoSearchRepository;

    public ShopInfoServiceImpl(ShopInfoRepository shopInfoRepository, ShopInfoMapper shopInfoMapper, ShopInfoSearchRepository shopInfoSearchRepository) {
        this.shopInfoRepository = shopInfoRepository;
        this.shopInfoMapper = shopInfoMapper;
        this.shopInfoSearchRepository = shopInfoSearchRepository;
    }

    /**
     * Save a shopInfo.
     *
     * @param shopInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ShopInfoDTO save(ShopInfoDTO shopInfoDTO) {
        log.debug("Request to save ShopInfo : {}", shopInfoDTO);

        ShopInfo shopInfo = shopInfoMapper.toEntity(shopInfoDTO);
        shopInfo = shopInfoRepository.save(shopInfo);
        ShopInfoDTO result = shopInfoMapper.toDto(shopInfo);
        shopInfoSearchRepository.save(shopInfo);
        return result;
    }

    /**
     * Get all the shopInfos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShopInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShopInfos");
        return shopInfoRepository.findAll(pageable)
            .map(shopInfoMapper::toDto);
    }



    /**
     *  get all the shopInfos where Shop is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ShopInfoDTO> findAllWhereShopIsNull() {
        log.debug("Request to get all shopInfos where Shop is null");
        return StreamSupport
            .stream(shopInfoRepository.findAll().spliterator(), false)
            .filter(shopInfo -> shopInfo.getShop() == null)
            .map(shopInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one shopInfo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShopInfoDTO> findOne(Long id) {
        log.debug("Request to get ShopInfo : {}", id);
        return shopInfoRepository.findById(id)
            .map(shopInfoMapper::toDto);
    }

    /**
     * Delete the shopInfo by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShopInfo : {}", id);
        shopInfoRepository.deleteById(id);
        shopInfoSearchRepository.deleteById(id);
    }

    /**
     * Search for the shopInfo corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShopInfoDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShopInfos for query {}", query);
        return shopInfoSearchRepository.search(queryStringQuery(query), pageable)
            .map(shopInfoMapper::toDto);
    }
}
