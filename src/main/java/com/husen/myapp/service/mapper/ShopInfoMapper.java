package com.husen.myapp.service.mapper;

import com.husen.myapp.domain.*;
import com.husen.myapp.service.dto.ShopInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ShopInfo and its DTO ShopInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {CouponsMapper.class})
public interface ShopInfoMapper extends EntityMapper<ShopInfoDTO, ShopInfo> {

    @Mapping(source = "coupons1.id", target = "coupons1Id")
    ShopInfoDTO toDto(ShopInfo shopInfo);

    @Mapping(source = "coupons1Id", target = "coupons1")
    @Mapping(target = "shop", ignore = true)
    ShopInfo toEntity(ShopInfoDTO shopInfoDTO);

    default ShopInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShopInfo shopInfo = new ShopInfo();
        shopInfo.setId(id);
        return shopInfo;
    }
}
