package com.husen.myapp.web.rest;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.codahale.metrics.annotation.Timed;
import com.husen.myapp.service.UserInfoService;
import com.husen.myapp.util.ShopConstant;
import com.husen.myapp.util.WXUtils;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 * REST controller for managing UserInfo.
 */
@RestController
@RequestMapping("/api")
public class WxUserInfoResource {

    private final Logger log = LoggerFactory.getLogger(WxUserInfoResource.class);

    private static final String ENTITY_NAME = "userInfo";

    private final UserInfoService userInfoService;

    public WxUserInfoResource(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }
    /**
     * 小程序入口用户注册	
     * @param userId //店铺所有人ID
     * @param appid
     * @param viewid
     * @param iv String	加密算法的初始向量，详细见加密数据解密算法
     * @param code String 用户code，可以换取用户openid
     * @param rawData String	不包括敏感信息的原始数据字符串，用于计算签名。
     * @param signature String	使用 sha1( rawData + sessionkey ) 得到字符串，用于校验用户信息，参考文档 signature。
     * @param encryptedData String	包括敏感数据在内的完整用户信息的加密数据，详细见加密数据解密算法
     * @return
     */
    @GetMapping("/register")
    @Timed 
    public ResponseEntity<Map> wxRegister(
	    		@RequestParam(required=false) String userId,
	    		@RequestParam(required=false) String appid,
	    		@RequestParam(required=false) String viewid,
	    		@RequestParam(required=false) String iv,
	    		@RequestParam(required=false) String code,
	    		@RequestParam(required=false) String rawData,
	    		@RequestParam(required=false) String signature,
	    		@RequestParam(required=false) String encryptedData
    		) {
        log.debug("REST request wxRegister : {} userId", userId);
        
        // 被加密的数据
        byte[] dataByte = Base64.decode(encryptedData);
        // 加密秘钥
       // byte[] keyByte = Base64.decode(sessionKey);
        // 偏移量
        byte[] ivByte = Base64.decode(iv); 
        
       // JSONObject info = WXUtils.getUserInfo(encryptedData,sessionKey,iv);
        
        
    	Map<String,Object> body = new HashMap<String,Object>();
    	
      
    	  
    	body.put(ShopConstant.RESULT_CODE, ShopConstant.RESULT_CODE_SUCCESS);
    	
    	
    	return ResponseEntity.ok().body(body);
    }

}
